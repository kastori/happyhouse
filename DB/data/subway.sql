INSERT INTO subway (no,line,name,address) VALUES (
'1','1호선','소요산','경기도 동두천시 평화로 2925');
INSERT INTO subway (no,line,name,address) VALUES (
'2','1호선','동두천','경기도 동두천시 평화로 2687');
INSERT INTO subway (no,line,name,address) VALUES (
'3','1호선','보산','경기도 동두천시 평화로 2539');
INSERT INTO subway (no,line,name,address) VALUES (
'4','1호선','동두천중앙','경기도 동두천시 동두천로 228');
INSERT INTO subway (no,line,name,address) VALUES (
'5','1호선','지행','경기도 동두천시 평화로 2285');
INSERT INTO subway (no,line,name,address) VALUES (
'6','1호선','덕정','경기도 양주시 화합로 1356');
INSERT INTO subway (no,line,name,address) VALUES (
'7','1호선','덕계','경기도 양주시 고덕로 139번길 317');
INSERT INTO subway (no,line,name,address) VALUES (
'8','1호선','양주','경기도 양주시 평화로 919');
INSERT INTO subway (no,line,name,address) VALUES (
'9','1호선','녹양','경기도 의정부시 평화로 757');
INSERT INTO subway (no,line,name,address) VALUES (
'10','1호선','가능','경기도 의정부시 평화로 633');
INSERT INTO subway (no,line,name,address) VALUES (
'11','1호선','의정부','경기도 의정부시 평화로 525');
INSERT INTO subway (no,line,name,address) VALUES (
'12','1호선','회룡','경기도 의정부시 평화로 363');
INSERT INTO subway (no,line,name,address) VALUES (
'13','1호선','망월사','경기도 의정부시 평화로 221');
INSERT INTO subway (no,line,name,address) VALUES (
'14','1호선','도봉산','서울특별시 도봉구 도봉로 964-33');
INSERT INTO subway (no,line,name,address) VALUES (
'15','1호선','도봉','서울특별시 도봉구 도봉로 170길 2');
INSERT INTO subway (no,line,name,address) VALUES (
'16','1호선','방학','서울특별시 도봉구 도봉로 150다길 3');
INSERT INTO subway (no,line,name,address) VALUES (
'17','1호선','창동','서울특별시 도봉구 마들로 11길 77');
INSERT INTO subway (no,line,name,address) VALUES (
'18','1호선','녹천','서울특별시 도봉구 덕릉로 376');
INSERT INTO subway (no,line,name,address) VALUES (
'19','1호선','월계','서울특별시 노원구 월계로 53길 40');
INSERT INTO subway (no,line,name,address) VALUES (
'20','1호선','광운대','서울특별시 노원구 석계로 98-2');
INSERT INTO subway (no,line,name,address) VALUES (
'21','1호선','석계','서울특별시 노원구 화랑로 341');
INSERT INTO subway (no,line,name,address) VALUES (
'22','1호선','신이문','서울특별시 동대문구 한천로 472');
INSERT INTO subway (no,line,name,address) VALUES (
'23','1호선','외대앞','서울특별시 동대문구 휘경로 27');
INSERT INTO subway (no,line,name,address) VALUES (
'24','1호선','회기','서울특별시 동대문구 회기로 196');
INSERT INTO subway (no,line,name,address) VALUES (
'25','1호선','청량리','서울특별시 동대문구 왕산로 205');
INSERT INTO subway (no,line,name,address) VALUES (
'26','1호선','제기동','서울특별시 동대문구 왕산로 93');
INSERT INTO subway (no,line,name,address) VALUES (
'27','1호선','신설동','서울특별시 동대문구 왕산로 93');
INSERT INTO subway (no,line,name,address) VALUES (
'28','1호선','동묘앞','서울특별시 종로구 종로 359');
INSERT INTO subway (no,line,name,address) VALUES (
'29','1호선','동대문','서울특별시 종로구 종로 302');
INSERT INTO subway (no,line,name,address) VALUES (
'30','1호선','종로3가','서울특별시 종로구 종로 129');
INSERT INTO subway (no,line,name,address) VALUES (
'31','1호선','시청','서울특별시 중구 세종대로 101');
INSERT INTO subway (no,line,name,address) VALUES (
'32','1호선','서울역','서울특별시 중구 세종대로 2');
INSERT INTO subway (no,line,name,address) VALUES (
'33','1호선','남영','서울특별시 용산구 한강대로 77길 25');
INSERT INTO subway (no,line,name,address) VALUES (
'34','1호선','용산','서울특별시 용산구 한강대로 23길 55');
INSERT INTO subway (no,line,name,address) VALUES (
'35','1호선','노량진','서울특별시 동작구 노량진로 151');
INSERT INTO subway (no,line,name,address) VALUES (
'36','1호선','대방','서울특별시 영등포구 여의대방로 300');
INSERT INTO subway (no,line,name,address) VALUES (
'37','1호선','신길','서울특별시 영등포구 영등포로 327');
INSERT INTO subway (no,line,name,address) VALUES (
'38','1호선','영등포','서울특별시 영등포구 경인로 102길 13');
INSERT INTO subway (no,line,name,address) VALUES (
'39','1호선','신도림','서울특별시 구로구 새말로 117-21');
INSERT INTO subway (no,line,name,address) VALUES (
'40','1호선','구로','서울특별시 구로구 구로중앙로 174');
INSERT INTO subway (no,line,name,address) VALUES (
'41','1호선','가산디지털단지','서울특별시 금천구 벚꽃로 309');
INSERT INTO subway (no,line,name,address) VALUES (
'42','1호선','독산','서울특별시 금천구 벚꽃로 115');
INSERT INTO subway (no,line,name,address) VALUES (
'43','1호선','금천구청','서울특별시 금천구 시흥대로 63길 91');
INSERT INTO subway (no,line,name,address) VALUES (
'44','1호선','광명','경기도 광명시 광명역로 21');
INSERT INTO subway (no,line,name,address) VALUES (
'45','1호선','석수','경기도 안양시 만안구 경수대로 1431');
INSERT INTO subway (no,line,name,address) VALUES (
'46','1호선','관악','경기도 안양시 만안구 경수대로 1273번길 46');
INSERT INTO subway (no,line,name,address) VALUES (
'47','1호선','안양','경기도 안양시 만안구 만안로 232');
INSERT INTO subway (no,line,name,address) VALUES (
'48','1호선','명학','경기도 안양시 만안구 만안로 20');
INSERT INTO subway (no,line,name,address) VALUES (
'49','1호선','금정','경기도 군포시 군포로 750');
INSERT INTO subway (no,line,name,address) VALUES (
'50','1호선','군포','경기도 군포시 군포역1길 27');
INSERT INTO subway (no,line,name,address) VALUES (
'51','1호선','당정','경기도 군포시 당정역로 91');
INSERT INTO subway (no,line,name,address) VALUES (
'52','1호선','의왕','경기도 의왕시 철도박물관로 66');
INSERT INTO subway (no,line,name,address) VALUES (
'53','1호선','성균관대','경기도 수원시 장안구 서부로 2149');
INSERT INTO subway (no,line,name,address) VALUES (
'54','1호선','화서','경기도 수원시 팔달구 덕영대로 692');
INSERT INTO subway (no,line,name,address) VALUES (
'55','1호선','수원','경기도 수원시 팔달구 덕영대로 924');
INSERT INTO subway (no,line,name,address) VALUES (
'56','1호선','세류','경기도 수원시 권선구 정조로 393-1');
INSERT INTO subway (no,line,name,address) VALUES (
'57','1호선','병점','경기도 화성시 떡전골로 97');
INSERT INTO subway (no,line,name,address) VALUES (
'58','1호선','서동탄','경기도 오산시 외삼미로 15번길 75-60');
INSERT INTO subway (no,line,name,address) VALUES (
'59','1호선','세마','경기도 오산시 세마역로 88');
INSERT INTO subway (no,line,name,address) VALUES (
'60','1호선','오산대','경기도 오산시 청학로 236');
INSERT INTO subway (no,line,name,address) VALUES (
'61','1호선','오산','경기도 오산시 역광장로 59');
INSERT INTO subway (no,line,name,address) VALUES (
'62','1호선','진위','경기도 평택시 경기대로 1855');
INSERT INTO subway (no,line,name,address) VALUES (
'63','1호선','송탄','경기도 평택시 경기대로 1855');
INSERT INTO subway (no,line,name,address) VALUES (
'64','1호선','서정리','경기도 평택시 탄현로 51');
INSERT INTO subway (no,line,name,address) VALUES (
'65','1호선','지제','경기도 평택시 경기대로 777');
INSERT INTO subway (no,line,name,address) VALUES (
'66','1호선','평택','경기도 평택시 평택로 51');
INSERT INTO subway (no,line,name,address) VALUES (
'67','1호선','성환','충청남도 천안시 서북구 성환읍 성환1로 237-5');
INSERT INTO subway (no,line,name,address) VALUES (
'68','1호선','직산','충청남도 천안시 서북구 직산읍 천안대로 1471-33');
INSERT INTO subway (no,line,name,address) VALUES (
'69','1호선','두정','충청남도 천안시 서북구 두정로 289');
INSERT INTO subway (no,line,name,address) VALUES (
'70','1호선','천안','충청남도 천안시 동남구 대흥로 239');
INSERT INTO subway (no,line,name,address) VALUES (
'71','1호선','봉명','충청남도 천안시 동남구 차돌로 51');
INSERT INTO subway (no,line,name,address) VALUES (
'72','1호선','쌍용','충청남도 천안시 서북구 쌍용19로 20');
INSERT INTO subway (no,line,name,address) VALUES (
'73','1호선','아산','충청남도 아산시 배방읍 희망로 90');
INSERT INTO subway (no,line,name,address) VALUES (
'74','1호선','배방','충청남도 아산시 배방읍 온천대로 1967');
INSERT INTO subway (no,line,name,address) VALUES (
'75','1호선','온양온천','충청남도 아산시 온천대로 1496');
INSERT INTO subway (no,line,name,address) VALUES (
'76','1호선','신창','충청남도 아산시 신창면 행목로 50');
INSERT INTO subway (no,line,name,address) VALUES (
'77','1호선','구일','서울특별시 구로구 구일로 133');
INSERT INTO subway (no,line,name,address) VALUES (
'78','1호선','개봉','서울특별시 구로구 경인로 40길 47');
INSERT INTO subway (no,line,name,address) VALUES (
'79','1호선','오류동','서울특별시 구로구 경인로 20길 13');
INSERT INTO subway (no,line,name,address) VALUES (
'80','1호선','온수','서울특별시 구로구 부일로 872');
INSERT INTO subway (no,line,name,address) VALUES (
'81','1호선','역곡','경기도 부천시 역곡로 1');
INSERT INTO subway (no,line,name,address) VALUES (
'82','1호선','소사','경기도 부천시 경인로 363');
INSERT INTO subway (no,line,name,address) VALUES (
'83','1호선','부천','경기도 부천시 부천로 1');
INSERT INTO subway (no,line,name,address) VALUES (
'84','1호선','중동','경기도 부천시 중동로 73');
INSERT INTO subway (no,line,name,address) VALUES (
'85','1호선','송내','경기도 부천시 송내대로 43');
INSERT INTO subway (no,line,name,address) VALUES (
'86','1호선','부개','인천광역시 부평구 수변로 22');
INSERT INTO subway (no,line,name,address) VALUES (
'87','1호선','부평','인천광역시 부평구 광장로 16');
INSERT INTO subway (no,line,name,address) VALUES (
'88','1호선','백운','인천광역시 부평구 마장로 55번길 14');
INSERT INTO subway (no,line,name,address) VALUES (
'89','1호선','동암','인천광역시 부평구 동암광장로 10');
INSERT INTO subway (no,line,name,address) VALUES (
'90','1호선','간석','인천광역시 남동구 석정로 522-14');
INSERT INTO subway (no,line,name,address) VALUES (
'91','1호선','주안','인천광역시 미추홀구 주안로 95-19');
INSERT INTO subway (no,line,name,address) VALUES (
'92','1호선','도화','인천광역시 미추홀구 숙골로 24번길 9');
INSERT INTO subway (no,line,name,address) VALUES (
'93','1호선','제물포','인천광역시 미추홀구 경인로 129');
INSERT INTO subway (no,line,name,address) VALUES (
'94','1호선','도원','인천광역시 동구 참외전로 245');
INSERT INTO subway (no,line,name,address) VALUES (
'95','1호선','동인천','인천광역시 중구 참외전로 121');
INSERT INTO subway (no,line,name,address) VALUES (
'96','1호선','인천','인천광역시 중구 제물량로 269');
INSERT INTO subway (no,line,name,address) VALUES (
'97','2호선','까치산','서울특별시 강서구 강서로 54');
INSERT INTO subway (no,line,name,address) VALUES (
'98','2호선','신정네거리','서울특별시 양천구 중앙로 261');
INSERT INTO subway (no,line,name,address) VALUES (
'99','2호선','양천구청','서울특별시 양천구 목동로 3길 33');
INSERT INTO subway (no,line,name,address) VALUES (
'100','2호선','도림천','서울특별시 구로구 경인로 67길 160');
INSERT INTO subway (no,line,name,address) VALUES (
'101','2호선','대림','서울특별시 구로구 도림천로 351');
INSERT INTO subway (no,line,name,address) VALUES (
'102','2호선','구로디지털단지','서울특별시 구로구 도림천로 477');
INSERT INTO subway (no,line,name,address) VALUES (
'103','2호선','신대방','서울특별시 동작구 대림로 2');
INSERT INTO subway (no,line,name,address) VALUES (
'104','2호선','신림','서울특별시 관악구 남부순환로 1614');
INSERT INTO subway (no,line,name,address) VALUES (
'105','2호선','봉천','서울특별시 관악구 남부순환로 1721');
INSERT INTO subway (no,line,name,address) VALUES (
'106','2호선','서울대입구','서울특별시 관악구 남부순환로 1822');
INSERT INTO subway (no,line,name,address) VALUES (
'107','2호선','낙성대','서울특별시 관악구 남부순환로 1928');
INSERT INTO subway (no,line,name,address) VALUES (
'108','2호선','사당','서울특별시 동작구 남부순환로 2089');
INSERT INTO subway (no,line,name,address) VALUES (
'109','2호선','방배','서울특별시 서초구 방배로 80');
INSERT INTO subway (no,line,name,address) VALUES (
'110','2호선','서초','서울특별시 서초구 서초대로 233');
INSERT INTO subway (no,line,name,address) VALUES (
'111','2호선','교대','서울특별시 서초구 서초대로 294');
INSERT INTO subway (no,line,name,address) VALUES (
'112','2호선','강남','서울특별시 강남구 강남대로 396');
INSERT INTO subway (no,line,name,address) VALUES (
'113','2호선','역삼','서울특별시 강남구 테헤란로 156');
INSERT INTO subway (no,line,name,address) VALUES (
'114','2호선','선릉','서울특별시 강남구 테헤란로 340');
INSERT INTO subway (no,line,name,address) VALUES (
'115','2호선','삼성','서울특별시 강남구 테헤란로 538');
INSERT INTO subway (no,line,name,address) VALUES (
'116','2호선','종합운동장','서울특별시 송파구 올림픽로 23');
INSERT INTO subway (no,line,name,address) VALUES (
'117','2호선','잠실새내','서울특별시 송파구 올림픽로 140');
INSERT INTO subway (no,line,name,address) VALUES (
'118','2호선','잠실','서울특별시 송파구 올림픽로 265');
INSERT INTO subway (no,line,name,address) VALUES (
'119','2호선','잠실나루','서울특별시 송파구 오금로 20');
INSERT INTO subway (no,line,name,address) VALUES (
'120','2호선','강변','서울특별시 광진구 강변역로 53');
INSERT INTO subway (no,line,name,address) VALUES (
'121','2호선','구의','서울특별시 광진구 아차산로 384-1');
INSERT INTO subway (no,line,name,address) VALUES (
'122','2호선','건대입구','서울특별시 광진구 아차산로 243');
INSERT INTO subway (no,line,name,address) VALUES (
'123','2호선','성수','서울특별시 성동구 아차산로 100');
INSERT INTO subway (no,line,name,address) VALUES (
'124','2호선','용답','서울특별시 성동구 용답길 86');
INSERT INTO subway (no,line,name,address) VALUES (
'125','2호선','신답','서울특별시 성동구 천호대로 232');
INSERT INTO subway (no,line,name,address) VALUES (
'126','2호선','용두','서울특별시 동대문구 천호대로 129');
INSERT INTO subway (no,line,name,address) VALUES (
'127','2호선','뚝섬','서울특별시 성동구 아차산로 18');
INSERT INTO subway (no,line,name,address) VALUES (
'128','2호선','한양대','서울특별시 성동구 왕십리로 206');
INSERT INTO subway (no,line,name,address) VALUES (
'129','2호선','왕십리','서울특별시 성동구 왕십리로 300');
INSERT INTO subway (no,line,name,address) VALUES (
'130','2호선','상왕십리','서울특별시 성동구 왕십리로 374');
INSERT INTO subway (no,line,name,address) VALUES (
'131','2호선','신당','서울특별시 중구 퇴계로 431-1');
INSERT INTO subway (no,line,name,address) VALUES (
'132','2호선','동대문역사문화공원','서울특별시 중구 을지로 279');
INSERT INTO subway (no,line,name,address) VALUES (
'133','2호선','을지로4가','서울특별시 중구 을지로 178');
INSERT INTO subway (no,line,name,address) VALUES (
'134','2호선','을지로3가','서울특별시 중구 을지로 106');
INSERT INTO subway (no,line,name,address) VALUES (
'135','2호선','을지로입구','서울특별시 중구 을지로 42');
INSERT INTO subway (no,line,name,address) VALUES (
'136','2호선','충청로','서울특별시 서대문구 서소문로 17');
INSERT INTO subway (no,line,name,address) VALUES (
'137','2호선','아현','서울특별시 마포구 신촌로 270');
INSERT INTO subway (no,line,name,address) VALUES (
'138','2호선','이대','서울특별시 마포구 신촌로 180');
INSERT INTO subway (no,line,name,address) VALUES (
'139','2호선','신촌','서울특별시 마포구 신촌로 90');
INSERT INTO subway (no,line,name,address) VALUES (
'140','2호선','홍대입구','서울특별시 마포구 양화로 160');
INSERT INTO subway (no,line,name,address) VALUES (
'141','2호선','합정','서울특별시 마포구 양화로 55');
INSERT INTO subway (no,line,name,address) VALUES (
'142','2호선','당산','서울특별시 영등포구 당산로 229');
INSERT INTO subway (no,line,name,address) VALUES (
'143','2호선','영등포구청','서울특별시 영등포구 당산로 121');
INSERT INTO subway (no,line,name,address) VALUES (
'144','2호선','문래','서울특별시 영등포구 당산로 28');
INSERT INTO subway (no,line,name,address) VALUES (
'145','3호선','대화','경기도 고양시 일산서구 중앙로 1569');
INSERT INTO subway (no,line,name,address) VALUES (
'146','3호선','주엽','경기도 고양시 일산서구 중앙로 1432');
INSERT INTO subway (no,line,name,address) VALUES (
'147','3호선','정발산','경기도 고양시 일산동구 중앙로 1270');
INSERT INTO subway (no,line,name,address) VALUES (
'148','3호선','마두','경기도 고양시 일산동구 중앙로 1180');
INSERT INTO subway (no,line,name,address) VALUES (
'149','3호선','백석','경기도 고양시 일산동구 중앙로 1180');
INSERT INTO subway (no,line,name,address) VALUES (
'150','3호선','대곡','경기도 고양시 덕양구 대주로 107번길 71-81');
INSERT INTO subway (no,line,name,address) VALUES (
'151','3호선','화정','경기도 고양시 덕양구 화정로 60');
INSERT INTO subway (no,line,name,address) VALUES (
'152','3호선','원당','경기도 고양시 덕양구 고양대로 1429');
INSERT INTO subway (no,line,name,address) VALUES (
'153','3호선','원흥','경기도 고양시 덕양구 권율대로 681');
INSERT INTO subway (no,line,name,address) VALUES (
'154','3호선','삼송','경기도 고양시 덕양구 삼송로 194');
INSERT INTO subway (no,line,name,address) VALUES (
'155','3호선','지축','경기도 고양시 덕양구 삼송로 300');
INSERT INTO subway (no,line,name,address) VALUES (
'156','3호선','구파발','서울특별시 은평구 진관2로 15-25');
INSERT INTO subway (no,line,name,address) VALUES (
'157','3호선','연신내','서울특별시 은평구 통일로 849');
INSERT INTO subway (no,line,name,address) VALUES (
'158','3호선','불광','서울특별시 은평구 통일로 723-1');
INSERT INTO subway (no,line,name,address) VALUES (
'159','3호선','녹번','서울특별시 은평구 통일로 602-1');
INSERT INTO subway (no,line,name,address) VALUES (
'160','3호선','홍제','서울특별시 서대문구 통일로 440-1');
INSERT INTO subway (no,line,name,address) VALUES (
'161','3호선','무악재','서울특별시 서대문구 통일로 361');
INSERT INTO subway (no,line,name,address) VALUES (
'162','3호선','독립문','서울특별시 서대문구 통일로 247');
INSERT INTO subway (no,line,name,address) VALUES (
'163','3호선','경복궁','서울특별시 종로구 사직로 130');
INSERT INTO subway (no,line,name,address) VALUES (
'164','3호선','안국','서울특별시 종로구 율곡로 62');
INSERT INTO subway (no,line,name,address) VALUES (
'165','3호선','충무로','서울특별시 중구 퇴계로 199');
INSERT INTO subway (no,line,name,address) VALUES (
'166','3호선','동대입구','서울특별시 중구 동호로 256');
INSERT INTO subway (no,line,name,address) VALUES (
'167','3호선','약수','서울특별시 중구 다산로 122');
INSERT INTO subway (no,line,name,address) VALUES (
'168','3호선','금호','서울특별시 성동구 동호로 104');
INSERT INTO subway (no,line,name,address) VALUES (
'169','3호선','옥수','서울특별시 성동구 동호로 21');
INSERT INTO subway (no,line,name,address) VALUES (
'170','3호선','압구정','서울특별시 강남구 압구정로 172');
INSERT INTO subway (no,line,name,address) VALUES (
'171','3호선','신사','서울특별시 강남구 도산대로 102');
INSERT INTO subway (no,line,name,address) VALUES (
'172','3호선','잠원','서울특별시 서초구 잠원로4길 46');
INSERT INTO subway (no,line,name,address) VALUES (
'173','3호선','고속터미널','서울특별시 서초구 신반포로 188');
INSERT INTO subway (no,line,name,address) VALUES (
'174','3호선','남부터미널','서울특별시 서초구 서초중앙로 31');
INSERT INTO subway (no,line,name,address) VALUES (
'175','3호선','양재','서울특별시 서초구 남부순환로 2585');
INSERT INTO subway (no,line,name,address) VALUES (
'176','3호선','매봉','서울특별시 강남구 남부순환로 2744');
INSERT INTO subway (no,line,name,address) VALUES (
'177','3호선','도곡','서울특별시 강남구 남부순환로 2814');
INSERT INTO subway (no,line,name,address) VALUES (
'178','3호선','대치','서울특별시 강남구 남부순환로 2952');
INSERT INTO subway (no,line,name,address) VALUES (
'179','3호선','학여울','서울특별시 강남구 남부순환로 3104');
INSERT INTO subway (no,line,name,address) VALUES (
'180','3호선','대청','서울특별시 강남구 일원로 2');
INSERT INTO subway (no,line,name,address) VALUES (
'181','3호선','일원','서울특별시 강남구 일원로 121');
INSERT INTO subway (no,line,name,address) VALUES (
'182','3호선','수서','서울특별시 강남구 광평로 270');
INSERT INTO subway (no,line,name,address) VALUES (
'183','3호선','가락시장','서울특별시 송파구 송파대로 257');
INSERT INTO subway (no,line,name,address) VALUES (
'184','3호선','경찰병원','서울특별시 송파구 중대로 149');
INSERT INTO subway (no,line,name,address) VALUES (
'185','3호선','오금','서울특별시 송파구 오금로 321');
INSERT INTO subway (no,line,name,address) VALUES (
'186','4호선','당고개','서울특별시 노원구 상계로 305');
INSERT INTO subway (no,line,name,address) VALUES (
'187','4호선','상계','서울특별시 노원구 상계로 182');
INSERT INTO subway (no,line,name,address) VALUES (
'188','4호선','노원','서울특별시 노원구 상계로 69-1');
INSERT INTO subway (no,line,name,address) VALUES (
'189','4호선','쌍문','서울특별시 도봉구 도봉로 486-1');
INSERT INTO subway (no,line,name,address) VALUES (
'190','4호선','수유','서울특별시 강북구 도봉로 338');
INSERT INTO subway (no,line,name,address) VALUES (
'191','4호선','미아','서울특별시 강북구 도봉로 198');
INSERT INTO subway (no,line,name,address) VALUES (
'192','4호선','미아사거리','서울특별시 강북구 도봉로 50');
INSERT INTO subway (no,line,name,address) VALUES (
'193','4호선','길음','서울특별시 성북구 동소문로 248');
INSERT INTO subway (no,line,name,address) VALUES (
'194','4호선','성신여대입구','서울특별시 성북구 동소문로 102');
INSERT INTO subway (no,line,name,address) VALUES (
'195','4호선','한성대입구','서울특별시 성북구 삼선교로 1');
INSERT INTO subway (no,line,name,address) VALUES (
'196','4호선','혜화','서울특별시 종로구 대학로 120');
INSERT INTO subway (no,line,name,address) VALUES (
'197','4호선','명동','서울특별시 중구 퇴계로 126');
INSERT INTO subway (no,line,name,address) VALUES (
'198','4호선','회현','서울특별시 중구 퇴계로 54');
INSERT INTO subway (no,line,name,address) VALUES (
'199','4호선','숙대입구','서울특별시 용산구 한강대로 306');
INSERT INTO subway (no,line,name,address) VALUES (
'200','4호선','삼각지','서울특별시 용산구 한강대로 180');
INSERT INTO subway (no,line,name,address) VALUES (
'201','4호선','신용산','서울특별시 용산구 한강대로 112');
INSERT INTO subway (no,line,name,address) VALUES (
'202','4호선','이촌','서울특별시 용산구 한강대로 112');
INSERT INTO subway (no,line,name,address) VALUES (
'203','4호선','동작','서울특별시 동작구 현충로 257');
INSERT INTO subway (no,line,name,address) VALUES (
'204','4호선','총신대입구','서울특별시 동작구 동작대로 117');
INSERT INTO subway (no,line,name,address) VALUES (
'205','4호선','남태령','서울특별시 서초구 과천대로 816');
INSERT INTO subway (no,line,name,address) VALUES (
'206','4호선','선바위','경기도 과천시 중앙로 440');
INSERT INTO subway (no,line,name,address) VALUES (
'207','4호선','경마공원','경기도 과천시 경마공원대로 105');
INSERT INTO subway (no,line,name,address) VALUES (
'208','4호선','대공원','경기도 과천시 대공원대로 50');
INSERT INTO subway (no,line,name,address) VALUES (
'209','4호선','과천','경기도 과천시 별양로 177');
INSERT INTO subway (no,line,name,address) VALUES (
'210','4호선','정부과천청사','경기도 과천시 중앙로 100');
INSERT INTO subway (no,line,name,address) VALUES (
'211','4호선','인덕원','경기도 안양시 동안구 흥안대로 529');
INSERT INTO subway (no,line,name,address) VALUES (
'212','4호선','평촌','경기도 안양시 동안구 부림로 123');
INSERT INTO subway (no,line,name,address) VALUES (
'213','4호선','범계','경기도 안양시 동안구 동안로 130');
INSERT INTO subway (no,line,name,address) VALUES (
'214','4호선','산본','경기도 군포시 번영로 504');
INSERT INTO subway (no,line,name,address) VALUES (
'215','4호선','수리산','경기도 군포시 번영로 385');
INSERT INTO subway (no,line,name,address) VALUES (
'216','4호선','대야미','경기도 군포시 대야1로 28');
INSERT INTO subway (no,line,name,address) VALUES (
'217','4호선','반월','경기도 안산시 상록구 건건로 119-10');
INSERT INTO subway (no,line,name,address) VALUES (
'218','4호선','상록수','경기도 안산시 상록구 상록수로 61');
INSERT INTO subway (no,line,name,address) VALUES (
'219','4호선','한대앞','경기도 안산시 상록구 충장로 337');
INSERT INTO subway (no,line,name,address) VALUES (
'220','4호선','중앙','경기도 안산시 단원구 중앙대로 918');
INSERT INTO subway (no,line,name,address) VALUES (
'221','4호선','고잔','경기도 안산시 단원구 중앙대로 784');
INSERT INTO subway (no,line,name,address) VALUES (
'222','4호선','초지','경기도 안산시 단원구 중앙대로 620');
INSERT INTO subway (no,line,name,address) VALUES (
'223','4호선','안산','경기도 안산시 단원구 중앙대로 462');
INSERT INTO subway (no,line,name,address) VALUES (
'224','4호선','신길온천','경기도 안산시 단원구 황고개로 2');
INSERT INTO subway (no,line,name,address) VALUES (
'225','4호선','정왕','경기도 시흥시 마유로 418번길 15');
INSERT INTO subway (no,line,name,address) VALUES (
'226','4호선','오이도','경기도 시흥시 역전로 430');
INSERT INTO subway (no,line,name,address) VALUES (
'227','5호선','마천','서울특별시 송파구 마천로 57길 7');
INSERT INTO subway (no,line,name,address) VALUES (
'228','5호선','거여','서울특별시 송파구 오금로 499');
INSERT INTO subway (no,line,name,address) VALUES (
'229','5호선','개롱','서울특별시 송파구 오금로 402');
INSERT INTO subway (no,line,name,address) VALUES (
'230','5호선','방이','서울특별시 송파구 양재대로 1127');
INSERT INTO subway (no,line,name,address) VALUES (
'231','5호선','올림픽공원','서울특별시 송파구 양재대로 1233');
INSERT INTO subway (no,line,name,address) VALUES (
'232','5호선','동촌동','서울특별시 강동구 양재대로 1369');
INSERT INTO subway (no,line,name,address) VALUES (
'233','5호선','강동','서울특별시 강동구 천호대로 1097');
INSERT INTO subway (no,line,name,address) VALUES (
'234','5호선','길동','서울특별시 강동구 양재대로 1480');
INSERT INTO subway (no,line,name,address) VALUES (
'235','5호선','굽은다리','서울특별시 강동구 양재대로 1572');
INSERT INTO subway (no,line,name,address) VALUES (
'236','5호선','명일','서울특별시 강동구 양재대로 1632');
INSERT INTO subway (no,line,name,address) VALUES (
'237','5호선','고덕','서울특별시 강동구 고덕로 253');
INSERT INTO subway (no,line,name,address) VALUES (
'238','5호선','상일동','서울특별시 강동구 고덕로 359');
INSERT INTO subway (no,line,name,address) VALUES (
'239','5호선','천호','서울특별시 강동구 천호대로 997');
INSERT INTO subway (no,line,name,address) VALUES (
'240','5호선','광나루','서울특별시 광진구 아차산로 571');
INSERT INTO subway (no,line,name,address) VALUES (
'241','5호선','아차산','서울특별시 광진구 천호대로 657');
INSERT INTO subway (no,line,name,address) VALUES (
'242','5호선','군자','서울특별시 광진구 천호대로 550');
INSERT INTO subway (no,line,name,address) VALUES (
'243','5호선','장한평','서울특별시 동대문구 천호대로 405');
INSERT INTO subway (no,line,name,address) VALUES (
'244','5호선','담십리','서울특별시 성동구 천호대로 300');
INSERT INTO subway (no,line,name,address) VALUES (
'245','5호선','마장','서울특별시 성동구 마장로 296');
INSERT INTO subway (no,line,name,address) VALUES (
'246','5호선','행당','서울특별시 성동구 행당로 89');
INSERT INTO subway (no,line,name,address) VALUES (
'247','5호선','신금호','서울특별시 성동구 금호로 154');
INSERT INTO subway (no,line,name,address) VALUES (
'248','5호선','청구','서울특별시 중구 청구로 77');
INSERT INTO subway (no,line,name,address) VALUES (
'249','5호선','광화문','서울특별시 종로구 세종대로 172');
INSERT INTO subway (no,line,name,address) VALUES (
'250','5호선','서대문','서울특별시 종로구 통일로 126');
INSERT INTO subway (no,line,name,address) VALUES (
'251','5호선','애오개','서울특별시 마포구 마포대로 210');
INSERT INTO subway (no,line,name,address) VALUES (
'252','5호선','공덕','서울특별시 마포구 마포대로 100');
INSERT INTO subway (no,line,name,address) VALUES (
'253','5호선','마포','서울특별시 마포구 마포대로 33');
INSERT INTO subway (no,line,name,address) VALUES (
'254','5호선','여의나루','서울특별시 영등포구 여의동로 343');
INSERT INTO subway (no,line,name,address) VALUES (
'255','5호선','여의도','서울특별시 영등포구 여의나루로 40');
INSERT INTO subway (no,line,name,address) VALUES (
'256','5호선','영등포시장','서울특별시 영등포구 양산로 200');
INSERT INTO subway (no,line,name,address) VALUES (
'257','5호선','양평','서울특별시 영등포구 양산로 21');
INSERT INTO subway (no,line,name,address) VALUES (
'258','5호선','오목교','서울특별시 양천구 오목로 342');
INSERT INTO subway (no,line,name,address) VALUES (
'259','5호선','목동','서울특별시 양천구 오목로 245');
INSERT INTO subway (no,line,name,address) VALUES (
'260','5호선','신정','서울특별시 양천구 오목로 179');
INSERT INTO subway (no,line,name,address) VALUES (
'261','5호선','화곡','서울특별시 강서구 화곡로 168');
INSERT INTO subway (no,line,name,address) VALUES (
'262','5호선','우장산','서울특별시 강서구 강서로 262');
INSERT INTO subway (no,line,name,address) VALUES (
'263','5호선','발산','서울특별시 강서구 공항대로 267');
INSERT INTO subway (no,line,name,address) VALUES (
'264','5호선','마곡','서울특별시 강서구 공항대로 163');
INSERT INTO subway (no,line,name,address) VALUES (
'265','5호선','송정','서울특별시 강서구 공항대로 33');
INSERT INTO subway (no,line,name,address) VALUES (
'266','5호선','김포공항','서울특별시 강서구 하늘길 77');
INSERT INTO subway (no,line,name,address) VALUES (
'267','5호선','개화산','서울특별시 강서구 양천로 22');
INSERT INTO subway (no,line,name,address) VALUES (
'268','5호선','방화','서울특별시 강서구 금낭화로 132');
INSERT INTO subway (no,line,name,address) VALUES (
'269','6호선','응암','서울특별시 은평구 증산로 477');
INSERT INTO subway (no,line,name,address) VALUES (
'270','6호선','역촌','서울특별시 은평구 서오릉로 63');
INSERT INTO subway (no,line,name,address) VALUES (
'271','6호선','독바위','서울특별시 은평구 불광로 129-1');
INSERT INTO subway (no,line,name,address) VALUES (
'272','6호선','구산','서울특별시 은평구 연서로 137-1');
INSERT INTO subway (no,line,name,address) VALUES (
'273','6호선','새절','서울특별시 은평구 증산로 400');
INSERT INTO subway (no,line,name,address) VALUES (
'274','6호선','증산','서울특별시 은평구 증산로 306');
INSERT INTO subway (no,line,name,address) VALUES (
'275','6호선','디지털미디어시티','서울특별시 은평구 수색로 175');
INSERT INTO subway (no,line,name,address) VALUES (
'276','6호선','월드컵경기장','서울특별시 마포구 월드컵로 240');
INSERT INTO subway (no,line,name,address) VALUES (
'277','6호선','마포구청','서울특별시 마포구 월드컵로 240');
INSERT INTO subway (no,line,name,address) VALUES (
'278','6호선','망원','서울특별시 마포구 월드컵로 240');
INSERT INTO subway (no,line,name,address) VALUES (
'279','6호선','상수','서울특별시 마포구 독막로 85');
INSERT INTO subway (no,line,name,address) VALUES (
'280','6호선','광흥창','서울특별시 마포구 독막로 165');
INSERT INTO subway (no,line,name,address) VALUES (
'281','6호선','대흥','서울특별시 마포구 대흥로 85');
INSERT INTO subway (no,line,name,address) VALUES (
'282','6호선','효창공원앞','서울특별시 용산구 백범로 287');
INSERT INTO subway (no,line,name,address) VALUES (
'283','6호선','녹사평','서울특별시 용산구 녹사평대로 195');
INSERT INTO subway (no,line,name,address) VALUES (
'284','6호선','이태원','서울특별시 용산구 이태원로 177');
INSERT INTO subway (no,line,name,address) VALUES (
'285','6호선','항강진','서울특별시 용산구 이태원로 287');
INSERT INTO subway (no,line,name,address) VALUES (
'286','6호선','버티고개','서울특별시 중구 다산로 38');
INSERT INTO subway (no,line,name,address) VALUES (
'287','6호선','창신','서울특별시 종로구 지봉로 112');
INSERT INTO subway (no,line,name,address) VALUES (
'288','6호선','보문','서울특별시 성북구 보문로 116');
INSERT INTO subway (no,line,name,address) VALUES (
'289','6호선','안암','서울특별시 성북구 인촌로 89');
INSERT INTO subway (no,line,name,address) VALUES (
'290','6호선','고려대','서울특별시 성북구 인촌로 89');
INSERT INTO subway (no,line,name,address) VALUES (
'291','6호선','월곡','서울특별시 성북구 월곡로 107');
INSERT INTO subway (no,line,name,address) VALUES (
'292','6호선','상월곡','서울특별시 성북구 화랑로 157');
INSERT INTO subway (no,line,name,address) VALUES (
'293','6호선','돌곶이','서울특별시 성북구 화랑로 243');
INSERT INTO subway (no,line,name,address) VALUES (
'294','6호선','태릉입구','서울특별시 노원구 동일로 992-1');
INSERT INTO subway (no,line,name,address) VALUES (
'295','6호선','화랑대','서울특별시 노원구 화랑로 510');
INSERT INTO subway (no,line,name,address) VALUES (
'296','6호선','봉화산','서울특별시 중랑구 신내로 232');
INSERT INTO subway (no,line,name,address) VALUES (
'297','6호선','신내','서울특별시 중랑구 용마산로136길 33');
INSERT INTO subway (no,line,name,address) VALUES (
'298','7호선','부평구청','인천광역시 부평구 부평대로 189');
INSERT INTO subway (no,line,name,address) VALUES (
'299','7호선','굴포천','인천광역시 부평구 길주로 623');
INSERT INTO subway (no,line,name,address) VALUES (
'300','7호선','삼산체육관','인천광역시 부평구 길주로 713');
INSERT INTO subway (no,line,name,address) VALUES (
'301','7호선','상동','경기도 부천시 길주로 104');
INSERT INTO subway (no,line,name,address) VALUES (
'302','7호선','부천시청','경기도 부천시 길주로 202');
INSERT INTO subway (no,line,name,address) VALUES (
'303','7호선','신중동','경기도 부천시 길주로 314');
INSERT INTO subway (no,line,name,address) VALUES (
'304','7호선','춘의','경기도 부천시 길주로 406');
INSERT INTO subway (no,line,name,address) VALUES (
'305','7호선','부천종합운동장','경기도 부천시 길주로 502');
INSERT INTO subway (no,line,name,address) VALUES (
'306','7호선','까치울','경기도 부천시 길주로 626');
INSERT INTO subway (no,line,name,address) VALUES (
'307','7호선','천왕','서울특별시 구로구 오리로 1154');
INSERT INTO subway (no,line,name,address) VALUES (
'308','7호선','광명사거리','경기도 광명시 오리로 980');
INSERT INTO subway (no,line,name,address) VALUES (
'309','7호선','철산','경기도 광명시 철산로 13');
INSERT INTO subway (no,line,name,address) VALUES (
'310','7호선','남구로','서울특별시 구로구 도림로 7');
INSERT INTO subway (no,line,name,address) VALUES (
'311','7호선','신풍','서울특별시 영등포구 신풍로 27');
INSERT INTO subway (no,line,name,address) VALUES (
'312','7호선','보라매','서울특별시 동작구 상도로 2');
INSERT INTO subway (no,line,name,address) VALUES (
'313','7호선','신대방삼거리','서울특별시 동작구 상도로 76');
INSERT INTO subway (no,line,name,address) VALUES (
'314','7호선','장승배기','서울특별시 동작구 상도로 188');
INSERT INTO subway (no,line,name,address) VALUES (
'315','7호선','상도','서울특별시 동작구 상도로 272');
INSERT INTO subway (no,line,name,address) VALUES (
'316','7호선','숭실대입구','서울특별시 동작구 상도로 378');
INSERT INTO subway (no,line,name,address) VALUES (
'317','7호선','남성','서울특별시 동작구 사당로 218');
INSERT INTO subway (no,line,name,address) VALUES (
'318','7호선','내방','서울특별시 서초구 서초대로 103');
INSERT INTO subway (no,line,name,address) VALUES (
'319','7호선','반포','서울특별시 서초구 신반포로 241');
INSERT INTO subway (no,line,name,address) VALUES (
'320','7호선','논현','서울특별시 강남구 학동로 102');
INSERT INTO subway (no,line,name,address) VALUES (
'321','7호선','학동','서울특별시 강남구 학동로 180');
INSERT INTO subway (no,line,name,address) VALUES (
'322','7호선','강남구청','서울특별시 강남구 학동로 346');
INSERT INTO subway (no,line,name,address) VALUES (
'323','7호선','청담','서울특별시 강남구 학동로 508');
INSERT INTO subway (no,line,name,address) VALUES (
'324','7호선','뚝섬유원지','서울특별시 광진구 능동로 10');
INSERT INTO subway (no,line,name,address) VALUES (
'325','7호선','어린이대공원','서울특별시 광진구 능동로 210');
INSERT INTO subway (no,line,name,address) VALUES (
'326','7호선','중곡','서울특별시 광진구 능동로 417');
INSERT INTO subway (no,line,name,address) VALUES (
'327','7호선','용마산','서울특별시 중랑구 용마산로 227');
INSERT INTO subway (no,line,name,address) VALUES (
'328','7호선','사가정','서울특별시 중랑구 사가정로 393');
INSERT INTO subway (no,line,name,address) VALUES (
'329','7호선','면목','서울특별시 중랑구 면목로 407');
INSERT INTO subway (no,line,name,address) VALUES (
'330','7호선','상봉','서울특별시 중랑구 망우로 297');
INSERT INTO subway (no,line,name,address) VALUES (
'331','7호선','중화','서울특별시 중랑구 동일로 797');
INSERT INTO subway (no,line,name,address) VALUES (
'332','7호선','먹골','서울특별시 중랑구 동일로 901');
INSERT INTO subway (no,line,name,address) VALUES (
'333','7호선','공릉','서울특별시 노원구 동일로 1074');
INSERT INTO subway (no,line,name,address) VALUES (
'334','7호선','하계','서울특별시 노원구 동일로 1196');
INSERT INTO subway (no,line,name,address) VALUES (
'335','7호선','중계','서울특별시 노원구 동일로 1308-1');
INSERT INTO subway (no,line,name,address) VALUES (
'336','7호선','마들','서울특별시 노원구 동일로 1530-1');
INSERT INTO subway (no,line,name,address) VALUES (
'337','7호선','수락산','서울특별시 노원구 동일로 1662');
INSERT INTO subway (no,line,name,address) VALUES (
'338','7호선','장암','경기도 의정부시 동일로 121');
INSERT INTO subway (no,line,name,address) VALUES (
'339','8호선','암사','서울특별시 강동구 올림픽로 776');
INSERT INTO subway (no,line,name,address) VALUES (
'340','8호선','강동구청','서울특별시 강동구 올림픽로 550');
INSERT INTO subway (no,line,name,address) VALUES (
'341','8호선','몽촌토성','서울특별시 송파구 올림픽로 383');
INSERT INTO subway (no,line,name,address) VALUES (
'342','8호선','석촌','서울특별시 송파구 송파대로 439');
INSERT INTO subway (no,line,name,address) VALUES (
'343','8호선','송파','서울특별시 송파구 송파대로 354');
INSERT INTO subway (no,line,name,address) VALUES (
'344','8호선','문정','서울특별시 송파구 송파대로 179');
INSERT INTO subway (no,line,name,address) VALUES (
'345','8호선','장지','서울특별시 송파구 송파대로 82');
INSERT INTO subway (no,line,name,address) VALUES (
'346','8호선','복정','서울특별시 송파구 송파대로 6');
INSERT INTO subway (no,line,name,address) VALUES (
'347','8호선','산성','경기도 성남시 수정구 수정로 365');
INSERT INTO subway (no,line,name,address) VALUES (
'348','8호선','남한산성입구','경기도 성남시 수정구 산성대로 445');
INSERT INTO subway (no,line,name,address) VALUES (
'349','8호선','단대오거리','경기도 성남시 수정구 산성대로 365');
INSERT INTO subway (no,line,name,address) VALUES (
'350','8호선','신흥','경기도 성남시 수정구 산성대로 280');
INSERT INTO subway (no,line,name,address) VALUES (
'351','8호선','수진','경기도 성남시 중원구 산성대로 200');
INSERT INTO subway (no,line,name,address) VALUES (
'352','8호선','모란','경기도 성남시 수정구 산성대로 100');
INSERT INTO subway (no,line,name,address) VALUES (
'353','9호선','개화','서울특별시 강서구 개화동로 8길 38');
INSERT INTO subway (no,line,name,address) VALUES (
'354','9호선','공항시장','서울특별시 강서구 방화동로 30');
INSERT INTO subway (no,line,name,address) VALUES (
'355','9호선','신방화','서울특별시 강서구 방화대로 301');
INSERT INTO subway (no,line,name,address) VALUES (
'356','9호선','마곡나루','서울특별시 강서구 마곡중앙5로 2');
INSERT INTO subway (no,line,name,address) VALUES (
'357','9호선','양천향교','서울특별시 강서구 양천로 341');
INSERT INTO subway (no,line,name,address) VALUES (
'358','9호선','가양','서울특별시 강서구 양천로 485');
INSERT INTO subway (no,line,name,address) VALUES (
'359','9호선','증미','서울특별시 강서구 양천로 560');
INSERT INTO subway (no,line,name,address) VALUES (
'360','9호선','등촌','서울특별시 강서구 공항대로 529');
INSERT INTO subway (no,line,name,address) VALUES (
'361','9호선','염창','서울특별시 강서구 공항대로 631');
INSERT INTO subway (no,line,name,address) VALUES (
'362','9호선','신목동','서울특별시 양천구 목동중앙로 217');
INSERT INTO subway (no,line,name,address) VALUES (
'363','9호선','선유도','서울특별시 영등포구 양평로 124');
INSERT INTO subway (no,line,name,address) VALUES (
'364','9호선','국회의사당','서울특별시 영등포구 국회대로 758');
INSERT INTO subway (no,line,name,address) VALUES (
'365','9호선','샛강','서울특별시 영등포구 의사당대로 166');
INSERT INTO subway (no,line,name,address) VALUES (
'366','9호선','노들','서울특별시 동작구 노량진로 238');
INSERT INTO subway (no,line,name,address) VALUES (
'367','9호선','흑석','서울특별시 동작구 현충로 90');
INSERT INTO subway (no,line,name,address) VALUES (
'368','9호선','구반포','서울특별시 서초구 신반포로 17');
INSERT INTO subway (no,line,name,address) VALUES (
'369','9호선','신반포','서울특별시 서초구 신반포로 105');
INSERT INTO subway (no,line,name,address) VALUES (
'370','9호선','사평','서울특별시 서초구 사평대로 285');
INSERT INTO subway (no,line,name,address) VALUES (
'371','9호선','신논현','서울특별시 강남구 봉은사로 102');
INSERT INTO subway (no,line,name,address) VALUES (
'372','9호선','언주','서울특별시 강남구 봉은사로 201');
INSERT INTO subway (no,line,name,address) VALUES (
'373','9호선','선정릉','서울특별시 강남구 선릉로 580');
INSERT INTO subway (no,line,name,address) VALUES (
'374','9호선','삼성중앙','서울특별시 강남구 봉은사로 501');
INSERT INTO subway (no,line,name,address) VALUES (
'375','9호선','봉은사','서울특별시 강남구 봉은사로 601');
INSERT INTO subway (no,line,name,address) VALUES (
'376','9호선','삼전','서울특별시 송파구 백제고분로 187');
INSERT INTO subway (no,line,name,address) VALUES (
'377','9호선','석촌고분','서울특별시 송파구 삼학사로 53');
INSERT INTO subway (no,line,name,address) VALUES (
'378','9호선','송파나루','서울특별시 송파구 백제고분로 446');
INSERT INTO subway (no,line,name,address) VALUES (
'379','9호선','한성백제','서울특별시 송파구 위례성대로 51');
INSERT INTO subway (no,line,name,address) VALUES (
'380','9호선','둔촌오륜','서울특별시 송파구 강동대로 327');
INSERT INTO subway (no,line,name,address) VALUES (
'381','9호선','중앙보훈병원','서울특별시 강동구 동남로');
