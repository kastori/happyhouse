-- ssafydb.house definition

CREATE TABLE `house` (
  `no` int NOT NULL DEFAULT '0',
  `dong` varchar(30) COLLATE utf8mb4_bin NOT NULL,
  `AptName` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `code` varchar(30) COLLATE utf8mb4_bin NOT NULL,
  `dealAmount` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `buildYear` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `dealMonth` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `dealYear` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `dealDay` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `area` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `floor` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `jibun` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `rentMoney` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `img` text COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

