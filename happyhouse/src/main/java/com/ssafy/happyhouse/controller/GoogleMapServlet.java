package com.ssafy.happyhouse.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.google.gson.Gson;
import com.ssafy.happyhouse.dto.LocationDto;


@Controller
public class GoogleMapServlet{
	
	@GetMapping("/GoogleMapServlet")
	private void getMap(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<LocationDto> list = new ArrayList<LocationDto>();
		list.add(new LocationDto("사직동", "광화문풍림스페이스본(101동~105동)", "37.5743822", "126.9688505"));
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(new Gson().toJson(list));
	}
}
