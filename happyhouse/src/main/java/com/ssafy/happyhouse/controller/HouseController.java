package com.ssafy.happyhouse.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.ssafy.happyhouse.dto.HouseDeal;
import com.ssafy.happyhouse.dto.PageMaker;
import com.ssafy.happyhouse.dto.SearchCriteria;
import com.ssafy.happyhouse.service.HouseService;

@Controller
@RequestMapping("/house")
public class HouseController {
	@Autowired
	HouseService service;

	@GetMapping("/list")
	   public String list(HttpServletRequest request, @ModelAttribute("scri") SearchCriteria scri , Model model) {
	      if(request.getParameterValues("select")!=null) {
	         String[] type = request.getParameterValues("select");         
	         scri.setSearchTypeArr(type);         
	         
	      }else {
	         String[] type = {"1","2","3","4"};
	         scri.setSearchTypeArr(type);
	      }
	      String[] address = scri.getKeyword().split(" ");
	      
	      scri.setKeyword(address[address.length-1]);
	      List<HouseDeal> list = service.searchAll(scri);
	      Gson gson = new Gson();
	      String jsonList = gson.toJson(list);
	      model.addAttribute("houseList",list);
	      model.addAttribute("jsonList",jsonList);
	      
	      PageMaker pageMaker = new PageMaker();
	      pageMaker.setCri(scri);
	      pageMaker.setTotalCount(service.getTotal(scri));      
	      model.addAttribute("pageMaker" , pageMaker);
	      
	      return "/house/HouseList";
	   }
	
	
	@GetMapping("/detail")
	public String detail(int no, Model model) {
		HouseDeal house = service.searchByNo(no);
		String jsonHouse = new Gson().toJson(house);
		model.addAttribute("house",house);
		model.addAttribute("jsonHouse",jsonHouse);
		return "/house/HouseDetail";
	}	
   @GetMapping("/subway")
   public String list3(HttpServletRequest request, @ModelAttribute("scri") SearchCriteria scri , Model model) {
      if(request.getParameterValues("select")!=null) {
         String[] type = request.getParameterValues("select");         
         scri.setSearchTypeArr(type);         
         
      }else {
         String[] type = {"1","2","3","4"};
         scri.setSearchTypeArr(type);
      }
      List<HouseDeal> list = service.searchAll(scri);
      model.addAttribute("houseList",list);
      
      PageMaker pageMaker = new PageMaker();
      pageMaker.setCri(scri);
      pageMaker.setTotalCount(service.getTotal(scri));      
      model.addAttribute("pageMaker" , pageMaker);
      
      return "/house/Subway";
   }
	
}
