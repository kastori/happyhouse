package com.ssafy.happyhouse.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.happyhouse.dto.HouseDeal;
import com.ssafy.happyhouse.dto.PageMaker;
import com.ssafy.happyhouse.dto.SearchCriteria;
import com.ssafy.happyhouse.service.HouseService;

@RestController
@RequestMapping("/houserest")
public class HouseRestController {
	@Autowired
	HouseService service;


	
	@GetMapping("/geoList")
	public List<HouseDeal> list(HttpServletRequest request, @ModelAttribute("scri") SearchCriteria scri,double lat,double lng , Model model) {
		if(request.getParameterValues("select")!=null) {
			String[] type = request.getParameterValues("select");			
			scri.setSearchTypeArr(type);			
			
		}else {
			String[] type = {"1","2","3","4"};
			scri.setSearchTypeArr(type);
		}
		System.out.println(lat+" "+lng);
	
		scri.setLat(lat);
		scri.setLng(lng);
		List<HouseDeal> list = service.searchByGoe(scri);
		model.addAttribute("houseList",list);
		
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(scri);
		pageMaker.setTotalCount(service.getTotal(scri));		
		model.addAttribute("pageMaker" , pageMaker);
		
		
		return list;
	}
	
}
