package com.ssafy.happyhouse.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.happyhouse.dto.Cctv;
import com.ssafy.happyhouse.service.CctvSerivice;

@RestController
public class CctvController {
	@Autowired
	CctvSerivice cctvSerivice;
	
	@GetMapping("/cctv")
	public List<Cctv> searchCctv(@RequestParam Map<String,Object> param) {
		param.forEach((key, value)
			    -> System.out.println("key: " + key + ", value: " + value));
		return cctvSerivice.searchCctv(param);
	}
}
