package com.ssafy.happyhouse.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ssafy.happyhouse.dto.Criteria;
import com.ssafy.happyhouse.dto.Notice;
import com.ssafy.happyhouse.dto.PageMaker;
import com.ssafy.happyhouse.dto.SearchCriteria;
import com.ssafy.happyhouse.service.NoticeService;


@Controller
@RequestMapping("/notice")
public class NoticeController{	
	
	@Autowired
	private NoticeService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(Model model, @ModelAttribute("scri") SearchCriteria scri) throws Exception{
		List<Notice> list = service.search(scri);		
		model.addAttribute("list", list);		
		
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(scri);
		pageMaker.setTotalCount(service.getTotal(scri));		
		model.addAttribute("pageMaker" , pageMaker);
		
		return "notice/NoticeList";		
	}
	
	@RequestMapping(value = "/regist", method = RequestMethod.GET)
	public String registForm(Notice notice,Model model) throws Exception{
		return "notice/NoticeRegistForm";		
	}
	
	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	public String regist(Notice notice) throws Exception{
		
		notice.setNoticeWriter("admin");
		System.out.println(notice.toString());
		service.insertNotice(notice);	
		return "redirect:/notice/list";		
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(int noticeNo,Model model) throws Exception{		
		
		model.addAttribute("notice",service.selectNoticeByNo(noticeNo));
		return "notice/NoticeDetail";		
	}
	

	@RequestMapping(value = "/remove", method = RequestMethod.GET)
	public String remove(int noticeNo,Model model) throws Exception{		
		service.deleteNotice(noticeNo);
		return "redirect:/notice/list";		
	}
	
	@RequestMapping(value = "/modify",method = RequestMethod.GET)
	public String modifyForm(int noticeNo,Model model ) {
		try {
			model.addAttribute("notice",service.selectNoticeByNo(noticeNo));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "notice/NoticeUpdateForm";
	}
	
	@RequestMapping(value = "/modify",method = RequestMethod.POST)
	public String modify(Notice notice) throws Exception{
		System.out.println(notice.toString());
		service.updateNotice(notice);
		
		return "redirect:/notice/list";		
	}
}
