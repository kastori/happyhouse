package com.ssafy.happyhouse.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ssafy.happyhouse.dto.Notice;
import com.ssafy.happyhouse.dto.PageMaker;
import com.ssafy.happyhouse.dto.SearchCriteria;
import com.ssafy.happyhouse.service.NoticeService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@Autowired
	private NoticeService service;	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model, @ModelAttribute("scri") SearchCriteria scri) {
		List<Notice> list = service.search(scri);		
		model.addAttribute("list", list);		
		
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(scri);
		pageMaker.setTotalCount(service.getTotal(scri));		
		model.addAttribute("pageMaker" , pageMaker);
		
		List<String> href = new ArrayList<String>();
		List<String> title = new ArrayList<String>();
		List<String> time = new ArrayList<String>();
		Document doc;
		try {
			doc = Jsoup.connect("https://realestate.daum.net/news/all").get();
			Elements newsHeadlines = doc.select("#live > ul > li > div > strong >a");
					
			int i =0;
			for (Element headline : newsHeadlines) {
				if(i<10) {
					href.add("https://realestate.daum.net/"+headline.attr("href"));				
					title.add(headline.text());	
			
					i++;					
				}
			}			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("href" , href);
		model.addAttribute("title" , title);
		
		return "index";
	}
	
	

}
