package com.ssafy.happyhouse.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ssafy.happyhouse.service.FavoriteService;

@Controller
@RequestMapping("/favo")
public class FavoriteController {
	
	@Autowired
	FavoriteService service;
	
	

	@GetMapping("/deletefavo")
	public String deletefavo(String fid){
		service.deleteFavorite(fid);
		return "redirect:/User/info";
	}
	
	
}
