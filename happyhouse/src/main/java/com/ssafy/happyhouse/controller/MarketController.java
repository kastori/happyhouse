package com.ssafy.happyhouse.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.happyhouse.dto.Market;
import com.ssafy.happyhouse.service.MarketService;

@RestController
public class MarketController {
	@Autowired
	MarketService marketService;
	
	@GetMapping("/market")
	public List<Market> searchMarket(String dong){
		System.out.println(dong);
		List<Market> list =  marketService.searchMarket(dong.trim());
		System.out.println(list);
		for(Market t : list) {
			System.out.println(t.toString());
		}
		return list;
	}
}
