package com.ssafy.happyhouse.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.happyhouse.dto.Subway;
import com.ssafy.happyhouse.service.SubwayService;

@RestController
public class SubwayRestContoller {

	@Autowired
	SubwayService service;
	
	@GetMapping("/test")
	public List<Subway>	getsubway(int no,int duration){
		
		return service.getStationList(no,duration);
	}
	
}
