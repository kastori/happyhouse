package com.ssafy.happyhouse.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssafy.happyhouse.dto.User;
import com.ssafy.happyhouse.service.UserService;

@Controller
@RequestMapping("/User")
public class UserController {	
	
	@Autowired
	UserService userService;	
	
	@RequestMapping(value = "/signupForm", method = RequestMethod.GET)
	String signupForm() {		
		return "user/signupForm";
	}
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	String signup(User user,Model model) throws Exception {
		userService.signUp(user);
		model.addAttribute("msg","회원가입이 완료되었습니다.");
		return "redirect:/";
	} 
	@RequestMapping(value = "/modfiy", method = RequestMethod.GET)
	String modfiyForm() {		
		return "user/updateForm";
	}
	
	@RequestMapping(value = "/modfiy", method = RequestMethod.POST)
	String modfiy(HttpSession session, User user,Model model) throws Exception{
		userService.updateUser(user);
		model.addAttribute("msg","수정이 완료되었습니다.");
		session.invalidate();
		return "redirect:/";
	}	
	@RequestMapping(value = "/remove", method = RequestMethod.GET)
	String remove(String userId,String userPwd, Model model,HttpSession session) throws Exception{
		User user=new User();
		user.setUserPwd(userPwd);
		user.setUserId(userId);
		userService.deleteUser(user);
		session.invalidate();
		model.addAttribute("msg","탈퇴 되었습니다.");
		return "redirect:/";
	}	

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	String info(String userid,Model model) throws Exception{		
		model.addAttribute("loginUser",userService.searchUserById(userid));
		return "user/userInfo";
	}
	
	//회원정보 검색
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	String search(String userId,Model model) throws Exception{		
		model.addAttribute("loginUser",userService.searchUserById(userId));
		return "user/userInfo";
	}
	

	//아이디 중복체크
	@ResponseBody
	@RequestMapping(value="/idChk", method = RequestMethod.POST)
	int idChk(String id) {	
		int result = userService.searchUserById(id) == null? 0:1;
	
		return result;
	}
}
