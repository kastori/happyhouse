package com.ssafy.happyhouse.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.happyhouse.dto.HouseInfo;
import com.ssafy.happyhouse.dto.SidoCode;
import com.ssafy.happyhouse.dto.Subway;
import com.ssafy.happyhouse.service.SelectBoxService;
import com.ssafy.happyhouse.service.SubwayService;



@RestController
public class SelectBoxController {

	@Autowired	
	private SelectBoxService service;

	@Autowired	
	private SubwayService subService;
	
	
	@RequestMapping("sido")
	public ResponseEntity<List<SidoCode>> sido(){
		try {
			return new ResponseEntity<>(service.selectSido(),HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	@RequestMapping("gugun")
	public ResponseEntity<List<SidoCode>> gugun(String sido){
		try {
			return new ResponseEntity<>(service.selectGugun(sido),HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	@RequestMapping("dong")
	public ResponseEntity<List<HouseInfo>> dong(String gugun){
		try {
			return new ResponseEntity<>(service.selectDong(gugun),HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	@RequestMapping("apt")
	public ResponseEntity<List<HouseInfo>> apt(String dong){
		try {
			return new ResponseEntity<>(service.selectApt(dong),HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	
	
	@RequestMapping("line")
	public ResponseEntity<List<Subway>> line(){
		try {
			return new ResponseEntity<>(subService.getLine(),HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}	
	@RequestMapping("no")
	public ResponseEntity<List<Subway>> no(String line){
		try {
			return new ResponseEntity<>(subService.getNo(line),HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	

}
