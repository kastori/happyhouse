package com.ssafy.happyhouse.controller;


import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssafy.happyhouse.dto.User;
import com.ssafy.happyhouse.service.LoginService;

@Controller
public class LoginController{
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(value="/login", method = RequestMethod.POST)
	String login(User user,HttpServletRequest request){		
		System.out.println(user);
		User loginUser=loginService.login(user);
		if(loginUser !=null) {
			HttpSession session= request.getSession();
			session.setAttribute("loginUser", loginUser);
		}
		// 사용자 정보가 없는 경우 
		else {
			request.setAttribute("errMsg", "아이디 또는 패스워드가 올바르지 않습니다.");
			return "redirect:/login";
		}		
		return "redirect:/"; 
	}
	@RequestMapping(value="/login", method = RequestMethod.GET)
	String loginForm(HttpServletRequest request){		
		return "user/login"; 
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}

}
