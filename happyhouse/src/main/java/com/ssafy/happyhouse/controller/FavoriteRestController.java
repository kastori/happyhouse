package com.ssafy.happyhouse.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.happyhouse.dto.Favorite;
import com.ssafy.happyhouse.service.FavoriteService;

@RestController
@RequestMapping("/favorite")
public class FavoriteRestController {
	
	@Autowired
	FavoriteService service;
	
	
	@GetMapping("/getfavo")
	public List<Favorite> getfavo(String id){
		List<Favorite> list=service.searchUserById(id);
		return list;
	}
	
	@PostMapping("/addfavo")
	public String addfavo(Favorite favo){
		if(favo.getId()=="") {
			return "로그인이 필요합니다";
		}
		List<Favorite> list=service.searchUserById(favo.getId());
		for(Favorite favorite:list) {
			if(favo.getNo()==favorite.getNo()) {
				return "이미 추가된 즐겨찾기입니다";
			}
		}
		service.addFavorite(favo);
		return "즐겨찾기 추가완료";
	}

	
	
}
