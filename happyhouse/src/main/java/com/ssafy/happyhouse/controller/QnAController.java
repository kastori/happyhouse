package com.ssafy.happyhouse.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.happyhouse.dto.Notice;
import com.ssafy.happyhouse.dto.PageMaker;
import com.ssafy.happyhouse.dto.QnA;
import com.ssafy.happyhouse.dto.SearchCriteria;
import com.ssafy.happyhouse.service.QnAService;


@Controller
@RequestMapping("/qna")
public class QnAController {
   @Autowired
   private QnAService qnaService;

   
   @GetMapping("list")
   public String searchQnA( @ModelAttribute("scri") SearchCriteria scri, Model model) {      
      List<QnA> list = qnaService.retrieveQnA(scri);      
      model.addAttribute("list", list);      
      
      PageMaker pageMaker = new PageMaker();
      pageMaker.setCri(scri);
      pageMaker.setTotalCount(qnaService.getCount(scri));      
      model.addAttribute("pageMaker" , pageMaker);
      
      return "/qna/QnaList";
   }

   @GetMapping("/detail")
   public String detailQnA(int no, Model model) {
      model.addAttribute("qna", qnaService.detailQnA(no));
      return "/qna/QnaDetail";
   }
    
    
   @GetMapping("/write")
   public String writeForm() {
      return "/qna/QnaWriteForm";
   }
   @PostMapping("/write")
   public String writeQnA(QnA qna) {
      qnaService.insertQnA(qna);
      return "redirect:/qna/list";
   }

   @GetMapping("/update")
   public String updateForm(int no, Model model) {
      model.addAttribute("qna", qnaService.detailQnA(no));
      return "qna/QnaUpdateForm";
   }
   @PostMapping("/update")
   public String updateQnA(QnA qna) {      
      qnaService.updateQnA(qna);
      return "redirect:/qna/list";
   }

   @GetMapping("/delete")
   public String deleteQnA(int no) {
      qnaService.deleteQnA(no);
      return "redirect:/qna/list";
   }
   
   @PostMapping("/reply")
   public String isnertReply(QnA qna) {
      System.out.println(qna);
      qnaService.insertReply(qna);      
      return "redirect:/qna/detail?no="+qna.getQnaNo();
   }
}