package com.ssafy.happyhouse.dto;

public class Favorite {

    private String id;
    private int    no;
    private int fid;
    private String AptName;
    private String dealAmount;
	@Override
	public String toString() {
		return "Favorite [id=" + id + ", no=" + no + ", fid=" + fid + ", aptName=" + AptName + ", dealAmount="
				+ dealAmount + "]";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public String getAptName() {
		return AptName;
	}
	public void setAptName(String aptName) {
		this.AptName = aptName;
	}
	public String getDealAmount() {
		return dealAmount;
	}
	public void setDealAmount(String dealAmount) {
		this.dealAmount = dealAmount;
	}

    
}