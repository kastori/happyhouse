package com.ssafy.happyhouse.dto;

public class Cctv {
	private int no;							
	private String mgrname;				//관리기관명
	private String streetaddr;			//소재지도로명주소
	private String jibunaddr;			//소재지지번주소
	private String usetype;				//설치목적구분
	private String carmeracnt;			//카메라대수
	private String pixel;				//카메라화소수
	private String direction;			//촬영방면정보
	private String storagedate;			//보관일수
	private String installationdate;	//설치년월
	private String phone;				//관리기관전화번호
	private double lat;					//위도
	private double lng;					//경도
	private String standarddate;		//데이터기준일자
		
	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getMgrname() {
		return mgrname;
	}

	public void setMgrname(String mgrname) {
		this.mgrname = mgrname;
	}

	public String getStreetaddr() {
		return streetaddr;
	}

	public void setStreetaddr(String streetaddr) {
		this.streetaddr = streetaddr;
	}

	public String getJibunaddr() {
		return jibunaddr;
	}

	public void setJibunaddr(String jibunaddr) {
		this.jibunaddr = jibunaddr;
	}

	public String getUsetype() {
		return usetype;
	}

	public void setUsetype(String usetype) {
		this.usetype = usetype;
	}

	public String getCarmeracnt() {
		return carmeracnt;
	}

	public void setCarmeracnt(String carmeracnt) {
		this.carmeracnt = carmeracnt;
	}

	public String getPixel() {
		return pixel;
	}

	public void setPixel(String pixel) {
		this.pixel = pixel;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getStoragedate() {
		return storagedate;
	}

	public void setStoragedate(String storagedate) {
		this.storagedate = storagedate;
	}

	public String getInstallationdate() {
		return installationdate;
	}

	public void setInstallationdate(String installationdate) {
		this.installationdate = installationdate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getStandarddate() {
		return standarddate;
	}

	public void setStandarddate(String standarddate) {
		this.standarddate = standarddate;
	}

	@Override
	public String toString() {
		return "Cctv [no=" + no + ", mgrname=" + mgrname + ", streetaddr=" + streetaddr + ", jibunaddr=" + jibunaddr
				+ ", usetype=" + usetype + ", carmeracnt=" + carmeracnt + ", pixel=" + pixel + ", direction="
				+ direction + ", storagedate=" + storagedate + ", installationdate=" + installationdate + ", phone="
				+ phone + ", lat=" + lat + ", lng=" + lng + ", standarddate=" + standarddate + "]";
	}
	
	
}
