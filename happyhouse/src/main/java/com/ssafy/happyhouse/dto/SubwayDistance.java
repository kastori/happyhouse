package com.ssafy.happyhouse.dto;

public class SubwayDistance {
	private int no1;
	private int no2;
	private int duration;
	@Override
	public String toString() {
		return "SubwayDistance [no1=" + no1 + ", no2=" + no2 + ", duration=" + duration + "]";
	}
	public int getNo1() {
		return no1;
	}
	public void setNo1(int no1) {
		this.no1 = no1;
	}
	public int getNo2() {
		return no2;
	}
	public void setNo2(int no2) {
		this.no2 = no2;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
}
