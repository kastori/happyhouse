package com.ssafy.happyhouse.dto;

public class Market {
	private String shopName;
	private String smallCategory;
	private String roadAddress;
	private double lat;
	private double lng;
	private String dong;
	
	public String getDong() {
		return dong;
	}
	
	public void setDong(String dong) {
		this.dong = dong;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getSmallCategory() {
		return smallCategory;
	}
	public void setSmallCategory(String smallCategory) {
		this.smallCategory = smallCategory;
	}
	public String getRoadAddress() {
		return roadAddress;
	}
	public void setRoadAddress(String roadAddress) {
		this.roadAddress = roadAddress;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return "Market [shopName=" + shopName + ", smallCategory=" + smallCategory + ", roadAddress=" + roadAddress
				+ ", lat=" + lat + ", lng=" + lng + ", dong=" + dong + "]";
	}
	
	
	
	
}
