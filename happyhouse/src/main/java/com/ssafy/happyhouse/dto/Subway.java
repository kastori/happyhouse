package com.ssafy.happyhouse.dto;

public class Subway {
	private int no;
	private String line;
	private String name;
	private String address;
	
	
	public Subway(int no, String line, String name, String address) {
		super();
		this.no = no;
		this.line = line;
		this.name = name;
		this.address = address;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
