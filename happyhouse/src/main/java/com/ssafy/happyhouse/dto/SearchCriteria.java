package com.ssafy.happyhouse.dto;

import java.util.Arrays;

public class SearchCriteria extends Criteria {

	private String searchType = ""; // 공지사항 검색 타입(String형)
	private String keyword = "";
	/**
	 * 아파트 타입 배열 
	 * 0 : 아파트 매매 
	 * 1 : 아파트 전월세 
	 * 2 : 다세대 매매 
	 * 3 : 다세대 전월세
	 */
	private String[] searchTypeArr;
	private double lng;
	private double lat;
	

	@Override
	public String toString() {
		return "SearchCriteria [searchType=" + searchType + ", keyword=" + keyword + ", searchTypeArr="
				+ Arrays.toString(searchTypeArr) + ", lng=" + lng + ", lat=" + lat + "]";
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String[] getSearchTypeArr() {
		return searchTypeArr;
	}

	public void setSearchTypeArr(String[] searchTypeArr) {
		this.searchTypeArr = searchTypeArr;
	}



}