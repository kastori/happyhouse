package com.ssafy.happyhouse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.SelectBoxDao;
import com.ssafy.happyhouse.dto.HouseInfo;
import com.ssafy.happyhouse.dto.SidoCode;

@Service
public class SelectBoxServiceImpl implements SelectBoxService {
	@Autowired
	SelectBoxDao dao;
	@Override
	public List<SidoCode> selectSido() throws Exception {
		// TODO Auto-generated method stub
		return dao.selectSido();
	}

	@Override
	public List<SidoCode> selectGugun(String sido) throws Exception {
		// TODO Auto-generated method stub
		return dao.selectGugun(sido);
	}

	@Override
	public List<HouseInfo> selectDong(String gugun) throws Exception {
		// TODO Auto-generated method stub
		return dao.selectDong(gugun);
	}

	@Override
	public List<HouseInfo> selectApt(String dong) throws Exception {
		// TODO Auto-generated method stub
		return dao.selectApt(dong);
	}
	
	
}
