package com.ssafy.happyhouse.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.NoticeDao;
import com.ssafy.happyhouse.dto.Notice;
import com.ssafy.happyhouse.dto.SearchCriteria;

@Service
public class NoticeServiceImpl implements NoticeService {
	@Autowired
	NoticeDao dao;
	
	@Override
	public List<Notice> search(SearchCriteria scri){
		return dao.selectNotice(scri);
	}

	@Override
	public Notice selectNoticeByNo(int noticeNo) {
		return dao.selectNoticeByNo(noticeNo);
	}

	@Override
	public void insertNotice(Notice notice) {
		dao.insertNotice(notice);
	}

	@Override
	public void updateNotice(Notice notice) {
		dao.updateNotice(notice);
	}

	@Override
	public void deleteNotice(int noticeNo) {
		dao.deleteNotice(noticeNo);
	}

	@Override
	public int viewCount(int noticeNo) {
		return 0;
	}

	@Override
	public int getTotal(SearchCriteria scri){
		return dao.getCount(scri);
	}
	
}

