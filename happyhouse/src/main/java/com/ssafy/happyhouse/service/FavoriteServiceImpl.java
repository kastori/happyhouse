package com.ssafy.happyhouse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.FavoriteDao;
import com.ssafy.happyhouse.dto.Favorite;

@Service
public class FavoriteServiceImpl implements FavoriteService {
	
	@Autowired	
	private FavoriteDao dao;
	
	@Override
	public void addFavorite(Favorite favorite) {		
		dao.insertFavo(favorite);		
	}

	@Override
	public void deleteFavorite(String fid) {		
		dao.deleteFavo(fid);		
	}

	@Override
	public List<Favorite> searchUserById(String id) {		
		return dao.selectFavoById(id);		
	}
	
	
}
