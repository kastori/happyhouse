package com.ssafy.happyhouse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.MarketDao;
import com.ssafy.happyhouse.dto.Market;

@Service
public class MarketServiceImpl implements MarketService{
	@Autowired
	MarketDao marketDao;

	@Override
	public List<Market> searchMarket(String dong) {
		return marketDao.selectMarketByDong(dong);
	}	
	
}
