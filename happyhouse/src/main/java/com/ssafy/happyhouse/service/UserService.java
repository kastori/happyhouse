package com.ssafy.happyhouse.service;

import com.ssafy.happyhouse.dto.User;

public interface UserService {
	void signUp(User user);
	void deleteUser(User user);
	void updateUser(User user);
	User searchUserById(String id);

}
