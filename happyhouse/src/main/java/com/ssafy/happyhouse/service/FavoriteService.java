package com.ssafy.happyhouse.service;

import java.util.List;

import com.ssafy.happyhouse.dto.Favorite;

public interface FavoriteService {
	void addFavorite(Favorite favorite);
	void deleteFavorite(String id);
	List<Favorite> searchUserById(String id);
}
