package com.ssafy.happyhouse.service;

import java.util.List;

import com.ssafy.happyhouse.dto.Market;

public interface MarketService {
	List<Market> searchMarket(String dong);

}
