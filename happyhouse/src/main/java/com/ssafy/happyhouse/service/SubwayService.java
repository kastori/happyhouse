package com.ssafy.happyhouse.service;

import java.util.List;

import com.ssafy.happyhouse.dto.Subway;

public interface SubwayService {
	public List<Subway> getStationList(int stationName,int duration);
	public List<Subway> getLine();
	public List<Subway> getNo(String line);
}
