package com.ssafy.happyhouse.service;

import java.util.List;

import com.ssafy.happyhouse.dto.QnA;
import com.ssafy.happyhouse.dto.SearchCriteria;

public interface QnAService {
	List<QnA> retrieveQnA(SearchCriteria scri);
	QnA detailQnA(int no);
	boolean insertQnA(QnA qna);
	boolean updateQnA(QnA qna);
	boolean deleteQnA(int no);
	int getCount(SearchCriteria scri);
	void insertReply(QnA qna);
}
