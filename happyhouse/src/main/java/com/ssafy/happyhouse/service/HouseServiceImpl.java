package com.ssafy.happyhouse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.HouseDao;
import com.ssafy.happyhouse.dto.HouseDeal;
import com.ssafy.happyhouse.dto.SearchCriteria;

@Service
public class HouseServiceImpl implements HouseService{
	@Autowired
	private HouseDao dao;
	
	/**
	 * 검색 조건(key) 검색 단어(word)에 해당하는 아파트 거래 정보(HouseInfo)를  검색해서 반환.  
	 * @param bean  검색 조건과 검색 단어가 있는 객체
	 * @return 조회한 식품 목록
	 */
	
	public List<HouseDeal> searchAll(SearchCriteria  scri){
		
		return dao.selectHouseDeal(scri);
	}
	
	/**
	 * 아파트 식별 번호에 해당하는 아파트 거래 정보를 검색해서 반환. 
	 * @param no	검색할 아파트 식별 번호
	 * @return		아파트 식별 번호에 해당하는 아파트 거래 정보를 찾아서 리턴한다, 없으면 null이 리턴됨
	 */
	public HouseDeal searchByNo(int no) {
		return dao.selectHouseDealByNo(no);
	}

	@Override
	public int getTotal(SearchCriteria scri) {
		// TODO Auto-generated method stub
		return dao.getCount(scri);
	}
	
	@Override
	public List<HouseDeal> searchByGoe(SearchCriteria scri) {
		return dao.selectGeo(scri);
	}
}




