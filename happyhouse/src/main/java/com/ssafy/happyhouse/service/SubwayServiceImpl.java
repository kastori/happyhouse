package com.ssafy.happyhouse.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.SubwayDao;
import com.ssafy.happyhouse.dao.SubwayDistanceDao;
import com.ssafy.happyhouse.dto.Subway;
import com.ssafy.happyhouse.dto.SubwayDistance;

@Service
public class SubwayServiceImpl implements SubwayService {

	@Autowired
	SubwayDao subwayDao;
	
	@Autowired
	SubwayDistanceDao distanceDao;
	
	
	public List<Subway> getStationList(int no,int duration) {
		List<Subway> tmp=null;
		
		int size=subwayDao.subwayCount();
		System.out.println(size);
		List<SubwayDistance> list=distanceDao.selectAll();
		tmp=subwayDao.selectAll();

		boolean[] check=new boolean[size+1];
		int[] distance=new int[size+1];
		int[][] node=new int[size+1][size+1];
		
		for(int i=0;i<list.size();i++) {
			node[list.get(i).getNo1()][list.get(i).getNo2()]=list.get(i).getDuration();
			node[list.get(i).getNo2()][list.get(i).getNo1()]=list.get(i).getDuration();
		}
		int max=Integer.MAX_VALUE;
		for(int i=0;i<=size;i++) {
			distance[i]=max;
		}
		for(int i=1;i<=size;i++) {
			if(node[no][i]!=0) {
				distance[i]=node[no][i];
			}
		}
		
		
		distance[no]=0;
		check[no]=true;
		
		for(int i=0;i<size-1;i++) {
			int index=-1;
			int min=Integer.MAX_VALUE;
			for(int j=1;j<=size;j++) {
				if(distance[j]<min&&!check[j]) {
					index=j;
					min=distance[j];
				}
			}
		

			check[index]=true;
			for(int j=1;j<=size;j++) {
				if(node[index][j]!=0&&!check[j]) {
					if(distance[j]>distance[index]+node[index][j]+2) {
						distance[j]=distance[index]+node[index][j]+2;
					}
				}
			}
		}
		
		List<Subway> result=new ArrayList<Subway>();
		
		for(int i=1;i<=size;i++) {
			if(distance[i]<=duration) {
				result.add(tmp.get(i-1));
			}
		}
		return result;
	}


	@Override
	public List<Subway> getLine() {
		return subwayDao.selectLine();
	}


	@Override
	public List<Subway> getNo(String line) {
		return subwayDao.selectByLine(line);
	}
	
	
	
	
	
	
	
}
