package com.ssafy.happyhouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.LoginDao;
import com.ssafy.happyhouse.dto.User;

@Service
public class LoginServiceImpl implements LoginService{
	
	@Autowired
	LoginDao dao;
	
	public User login(User user) {
		return dao.loginCheck(user);		
	}	
}
