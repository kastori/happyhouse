package com.ssafy.happyhouse.service;

import java.util.List;

import com.ssafy.happyhouse.dto.HouseInfo;
import com.ssafy.happyhouse.dto.SidoCode;

public interface SelectBoxService {
	List<SidoCode> selectSido() throws Exception;
	List<SidoCode> selectGugun(String sido) throws Exception;
	List<HouseInfo> selectDong(String gugun) throws Exception;
	List<HouseInfo> selectApt(String dong) throws Exception;
}
