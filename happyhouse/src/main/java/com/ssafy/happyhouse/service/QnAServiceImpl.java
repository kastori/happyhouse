package com.ssafy.happyhouse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.QnADao;
import com.ssafy.happyhouse.dto.QnA;
import com.ssafy.happyhouse.dto.SearchCriteria;

@Service
public class QnAServiceImpl implements QnAService {

	@Autowired
	private QnADao qnaDao;

	@Override
	public List<QnA> retrieveQnA(SearchCriteria scri) {
		return qnaDao.selectQnA(scri);
	}

	@Override
	public QnA detailQnA(int no) {
		return qnaDao.selectQnAByNo(no);
	}

	@Override
	public boolean insertQnA(QnA qna) {
		return qnaDao.insertQnA(qna);
	}

	@Override
	public boolean updateQnA(QnA qna) {
		return qnaDao.updateQnA(qna);
	}

	@Override
	public boolean deleteQnA(int no) {
		return qnaDao.deleteQnA(no);
	}

	@Override
	public int getCount(SearchCriteria scri) {
		return qnaDao.getCount(scri);
	}

	@Override
	public void insertReply(QnA qna) {
		qnaDao.insertReply(qna);
	}

	
}
