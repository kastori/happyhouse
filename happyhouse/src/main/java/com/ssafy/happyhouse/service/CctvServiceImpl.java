package com.ssafy.happyhouse.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.CctvDao;
import com.ssafy.happyhouse.dto.Cctv;

@Service
public class CctvServiceImpl implements CctvSerivice{
	@Autowired
	CctvDao cctvDao;
	
	@Override
	public List<Cctv> searchCctv(Map<String, Object> param) {		
		return cctvDao.selectCctvByLatLng(param);
	}

}
