package com.ssafy.happyhouse.service;

import java.util.List;

import com.ssafy.happyhouse.dto.Notice;
import com.ssafy.happyhouse.dto.SearchCriteria;

public interface NoticeService {

	public List<Notice> search(SearchCriteria scri);

	public Notice selectNoticeByNo(int noticeNo);

	public void insertNotice(Notice notice);

	public void updateNotice(Notice notice);

	public void deleteNotice(int noticeNo);

	public int viewCount(int noticeNo);

	public int getTotal(SearchCriteria scri);
}
