package com.ssafy.happyhouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.dao.UserDao;
import com.ssafy.happyhouse.dto.User;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao dao;

	@Override
	public void signUp(User user){
		dao.insertUser(user);
	}

	@Override
	public void deleteUser(User user){
		dao.deleteUser(user);
		
	}

	@Override
	public void updateUser(User user){
		dao.updateUser(user);		
	}

	@Override
	public User searchUserById(String id){		
		return dao.selectUserById(id);
	}
	
}
