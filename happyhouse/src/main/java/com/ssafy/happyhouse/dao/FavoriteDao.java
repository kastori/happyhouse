package com.ssafy.happyhouse.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafy.happyhouse.dto.Favorite;

@Mapper
public interface FavoriteDao {
	void insertFavo(Favorite favorite);
	void deleteFavo(String fid);
	List<Favorite> selectFavoById(String id);
}
