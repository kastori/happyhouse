package com.ssafy.happyhouse.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.ssafy.happyhouse.dto.Cctv;

@Mapper
public interface CctvDao {
	//지도의 위도경로 범위 사이에 있는 cctv 검색
	List<Cctv> selectCctvByLatLng(Map<String, Object> map);
}
