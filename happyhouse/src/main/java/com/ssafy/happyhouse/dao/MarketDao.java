package com.ssafy.happyhouse.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafy.happyhouse.dto.Market;

@Mapper
public interface MarketDao {
	List<Market> selectMarketByDong(String dong);
}
