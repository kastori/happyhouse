package com.ssafy.happyhouse.dao;

import org.apache.ibatis.annotations.Mapper;

import com.ssafy.happyhouse.dto.User;

@Mapper
public interface UserDao {
	void insertUser(User user);
	void deleteUser(User user);
	void updateUser(User user);
	User selectUserById(String id);
}
