package com.ssafy.happyhouse.dao;

import java.util.List;

import com.ssafy.happyhouse.dto.Subway;

public interface SubwayDao {
	List<Subway> selectAll();
	int subwayCount();
	List<Subway> selectLine();
	List<Subway> selectByLine(String line);
	
}
