package com.ssafy.happyhouse.dao;

import java.util.List;

import com.ssafy.happyhouse.dto.QnA;
import com.ssafy.happyhouse.dto.SearchCriteria;

public interface QnADao {
	List<QnA> selectQnA(SearchCriteria scri);
	QnA selectQnAByNo(int no);
	boolean insertQnA(QnA qna);
	boolean updateQnA(QnA qna);
	boolean deleteQnA(int no);
	int getCount(SearchCriteria scri);
	void insertReply(QnA qna);
}
