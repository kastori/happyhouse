package com.ssafy.happyhouse.dao;


import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafy.happyhouse.dto.Notice;
import com.ssafy.happyhouse.dto.SearchCriteria;

@Mapper
public interface NoticeDao {
	//공지사항 목록 조회
	public List<Notice> selectNotice(SearchCriteria scri);
	
	//공지사항 총 갯수
	public int getCount(SearchCriteria scri);
	
	//공지사항 번호로 조회
	public Notice selectNoticeByNo(int noticeNo);
	
	//공지사항 수정
	public void updateNotice(Notice notice);
	
	//공지사항 등록
	public void insertNotice(Notice notice);
	
	//공지사항 삭제
	public void deleteNotice(int noticeNo);
	
	
}

