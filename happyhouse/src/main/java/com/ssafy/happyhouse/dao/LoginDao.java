package com.ssafy.happyhouse.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ssafy.happyhouse.dto.User;

@Mapper
public interface LoginDao {	
	User loginCheck(User user);
	 
}
