package com.ssafy.happyhouse.dao;

import java.util.List;

import com.ssafy.happyhouse.dto.SubwayDistance;

public interface SubwayDistanceDao {
	List<SubwayDistance> selectAll();
}
