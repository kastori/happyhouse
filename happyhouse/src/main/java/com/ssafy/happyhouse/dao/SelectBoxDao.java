package com.ssafy.happyhouse.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafy.happyhouse.dto.HouseInfo;
import com.ssafy.happyhouse.dto.SidoCode;

@Mapper
public interface SelectBoxDao {
	List<SidoCode> selectSido() throws Exception;
	List<SidoCode> selectGugun(String sido) throws Exception;
	List<HouseInfo> selectDong(String gugun) throws Exception;
	List<HouseInfo> selectApt(String dong) throws Exception;
}
