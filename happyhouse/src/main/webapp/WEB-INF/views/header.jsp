<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
	
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="/">HappyHouse</a>
  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
 	  		<ul class="navbar-nav ml-auto mt-2 mt-lg-0">                
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/house/list"/>">아파트 시세</a>
				</li>
				<li class="nav-item">
	               <a class="nav-link" href="<c:url value="/house/subway"/>">지하철 거리 조회</a>
	            </li>      
				<li class="nav-item">
					<a class="nav-link"	href="<c:url value="/notice/list"/>">공지사항</a>
				</li>
				<li class="nav-item">
					<a class="nav-link"	href="<c:url value="/qna/list"/>">문의사항</a>
				</li>	    
				<c:choose>
					<c:when test="${empty loginUser}">
						<li class="nav-item">
							<a class="nav-link" href="<c:url value="/login"/>">로그인</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<c:url value="/User/signupForm"/>">회원가입</a>
						</li>
					</c:when>
					<c:otherwise>
						<div class="btn-group">
						  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						    ${loginUser.userId } <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li>	
						    	<a class="nav-link" href="<c:url value="/User/info"/>">회원 정보 조회</a>
							</li>
						    <li>
						    	<a class="nav-link"	href="<c:url value="/User/modfiy"/>">회원 정보 수정</a>						    	
							</li>	
							<li>
								<a class="nav-link"	href="<c:url value="/logout"/>">로그아웃</a>
							</li>		
											    						    
						  </ul>
						</div>												
					</c:otherwise>
				</c:choose>
	        </ul>    
    
    	</div> 	  		
	  	
	  </div>
</nav>