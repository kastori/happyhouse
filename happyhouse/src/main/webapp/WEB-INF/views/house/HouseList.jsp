<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%
   String contextPath = request.getContextPath();
%>   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <title>HappyHouse | 아파트 시세</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
   <script   src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   
     <!-- Bootstrap core CSS -->
     <link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

     <!-- Custom fonts for this template -->
     <link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
     <link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
     <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

     
     <!-- Custom styles for this template -->
     <link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">   
      
      <style>
         #map-container {
        position: relative;
        height: 100%;
      }
      #map{
        height: 100vh;
        width: 100%;
        min-height: 100%;
        max-height: none;   
        }
      .card {
           margin: 0 auto; /* Added */
           float: none; /* Added */
           margin-bottom: 10px; /* Added */
      }
      </style>
</head>
<body>
<div class="wrap">
   <%@ include file="../header.jsp" %>
   <div class="container-fluid">
      <div class="row mt-3 mb-3 justify-content-center">
         <form method="get" action="<c:url value="/house/list"/>">
            <div class="row">
               <label class="checkbox-inline">
                  <input type="checkbox" name="select" value="1" checked="checked"> 아파트 매매                  
               </label>                     
               <label class="checkbox-inline">
                     <input type="checkbox" name="select" value="2" checked="checked"> 아파트 전월세                  
               </label>
                     
               <label class="checkbox-inline">
                     <input type="checkbox" name="select" value="3" checked="checked"> 다세대,주택 매매                  
               </label>
               <label class="checkbox-inline">
                     <input type="checkbox" name="select" value="4" checked="checked"> 다세대,주택  전월세               
               </label>               
            </div>      
            <div class="row">         
                  <div class="form-inline justify-content-center">                     
                   <select class="form-control" name="searchType">
                      <option value="n"<c:out value="${scri.searchType == null ? 'selected' : ''}"/>>-----</option>
                      <option value="ad"<c:out value="${scri.searchType eq 'ad' ? 'selected' : ''}"/>>all</option>
                      <option value="a"<c:out value="${scri.searchType eq 'a' ? 'selected' : ''}"/>>아파트이름</option>
                      <option value="d"<c:out value="${scri.searchType eq 'd' ? 'selected' : ''}"/>>동이름</option>                        
                   </select>
                   <div class="col-xs-5">
                      <input type="text" class="form-control " name="keyword" id="keywordInput" value="${scri.keyword}"/>                         
                   </div>
                  <button class="form-control">검색</button>
               </div>
            </div>            
         </form>
      </div>
      <div class="row">
           <div class="col-md-4 order-lg-1 my-auto">
              <div class="row">
                 <c:forEach var="HouseDeal" items="${houseList}">
                <div class="col-md-6 align-self-center">
                       <div class="card card-block" style="width: 15rem;">
                          <c:choose>
                             <c:when test="${empty HouseDeal.img }">
                                <img src="<c:url value='/static/img/no_img.png'/>" class="card-img-top" style="height: 150px" alt="이미지준비중">                                   
                             </c:when>
                             <c:otherwise>
                                <img src="<c:url value='/static/img/${HouseDeal.img}'/>" class="card-img-top" style="height: 150px" alt="${HouseDeal.aptName}">      
                             </c:otherwise>
                          </c:choose>
                          
                    <div class="card-body">
                      <p class="card-text">${HouseDeal.dong}</p>
                      <h5 class="card-title">${HouseDeal.dealAmount}만원</h5>                            
                       <p class="card-text">${HouseDeal.aptName}</p>
                     <a  href="<c:url value="/house/detail?no=${HouseDeal.no}"/>">자세히</a>                    
                    </div>
                  </div> 
               </div>   
            </c:forEach>                  
               </div>   
         <div class="row justify-content-center">
           <nav class="justify-content-center">
               <ul class="pagination ">
                <c:if test="${pageMaker.prev}">
                   <li class="page-item">
                      <a class="page-link" href="list${pageMaker.makeSearch(pageMaker.startPage - 1)}">
                         <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>   
                     </a>
                  </li>
                </c:if> 
            
                <c:forEach begin="${pageMaker.startPage}" end="${pageMaker.endPage}" var="idx">
                   <li class="page-item"><a class="page-link" href="list${pageMaker.makeSearch(idx)}">${idx}</a></li>
                </c:forEach>
            
                <c:if test="${pageMaker.next && pageMaker.endPage > 0}">
                   <li>
                      <a class="page-link" href="list${pageMaker.makeSearch(pageMaker.endPage + 1)}">
                          <span aria-hidden="true">&raquo;</span>
                      </a>
                   </li>
                </c:if> 
              </ul>
           </nav>
             </div>   
          </div>                         
         <div id="#map-container" class="col-md-8 order-lg-2">
             <div id="map" ></div>
               <!-- map start -->
                 
               <script
                  src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
               <script async defer
                  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE&callback=initMap"></script>
               <script>
                  var multi = {lat: 37.566535, lng: 126.9779692};
                  var map;
                  var markers = [];
                  var test = ${jsonList};

                  function initMap() {
                     map = new google.maps.Map(document.getElementById('map'), {
                        center: multi, zoom: 12
                     });
                     
                     ${jsonList}.forEach(function(HouseDeal) {
                           addMarker(HouseDeal.lat,HouseDeal.lng,HouseDeal.aptName,1);
                       });
                                      
                  }
                  function addMarker(tmpLat, tmpLng, aptName) {
                     var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(parseFloat(tmpLat),parseFloat(tmpLng)),
                        label: aptName,
                        title: aptName,
                        icon: new google.maps.MarkerImage("/static/img/house.png", null, null, null, new google.maps.Size(30,30))
                     });
                     marker.addListener('click', function() {
                        map.setZoom(15);
                        map.setCenter(marker.getPosition());
                     });
                     
                     marker.setMap(map);
                    
                  }
                  function geocode(addr) {             
                      $.get("https://maps.googleapis.com/maps/api/geocode/json"
                            ,{   key:'AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE'
                               , address:addr
                            }
                            , function(data, status) {
                                  map.setCenter(data.results[0].geometry.location);
                                  map.setZoom(18);
                                    addMarker(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng, `${house.aptName}`,4);
                            }
                            , "json"
                      );//get                
                    }                    
               </script>               
          </div>         
      
      
      </div>
      </div><!-- container -->   
   </div>   <!-- wrap -->
    <script src="<c:url value='/static/vendor/jquery/jquery.min.js'/>"></script>
  <script src="<c:url value= '/static/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>      
   
<footer class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
          <ul class="list-inline mb-2">
            <li class="list-inline-item">
              <a href="#">About</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#">Contact</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
          </ul>
          <p class="text-muted small mb-4 mb-lg-0">&copy; Your Website 2019. All Rights Reserved.</p>
        </div>
        <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
          <ul class="list-inline mb-0">
            <li class="list-inline-item mr-3">
              <a href="#">
                <i class="fab fa-facebook fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item mr-3">
              <a href="#">
                <i class="fab fa-twitter-square fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-instagram fa-2x fa-fw"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>   


      
</body>
</html>