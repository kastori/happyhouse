<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
   String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HappyHouse | 지하철 거리 시세</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
<script
   src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Bootstrap core CSS -->
<link
   href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>"
   rel="stylesheet">

<!-- Custom fonts for this template -->
<link
   href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>"
   rel="stylesheet">
<link
   href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>"
   rel="stylesheet" type="text/css">
<link
   href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic"
   rel="stylesheet" type="text/css">


<!-- Custom styles for this template -->
<link href="<c:url value='/static/css/landing-page.min.css'/>"
   rel="stylesheet">

<!-- Google Maps JavaScript library -->
<script
   src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE"></script>

<style type="text/css">
#station {
   overflow-x: scroll;
   white-space: nowrap;
   height: 150px;
}

.card {
   margin: 0 auto; /* Added */
   float: none; /* Added */
   margin-bottom: 10px; /* Added */
}

li {
   list-style: none;
   float: left;
   padding: 6px;
}

#map {
   width: 90%;
   height: 1700px;
}

.line {
   height: 100%;
   width: 200px;
   border: 1px solid #bcbcbc;
   display: inline-block;
   margin-left: 10px;
}
#housedealList{
   height: 1700px;
overflow-y: scroll;
}
</style>


</head>
<body>
<div class="wrap">
   <%@ include file="../header.jsp"%>
   <div class="row" style="margin-left:10px">
      <div class="form-inline ">
         <div class="search form-group">
            호선 : <select class="form-control" id="line">
               <option value="0">선택</option>
            </select> 지하철역 : <select class="form-control" id="no">
               <option value="0">선택</option>
            </select> 시간 : <input class="form-control" type="text" name="duration"
               id="duration" />
            <button class="form-control" id="getSubwayList">검색</button>
         </div>
      </div>
   </div>

   <div id="station"></div>



   <section class="showcase">
      <div class="container-fluid p-0">
         <div class="row no-gutters">
            <div class="col-md-4 order-lg-1 my-auto">
               <div class="row" id="housedealList">
                  <c:forEach var="HouseDeal" items="${houseList}">
                     <div class="col-md-6">
                        <div class="card" style="width: 15rem;">
                           <img src="<c:url value='/static/img/no_img.png'/>"
                              class="card-img-top" style="height: 150px" alt="...">
                           <div class="card-body">
                              <p class="card-text">${HouseDeal.dong}</p>
                              <h5 class="card-title">${HouseDeal.dealAmount}만원</h5>
                              <p class="card-text">${HouseDeal.aptName}</p>
                              <a href="<c:url value="/house/detail?no=${HouseDeal.no}"/>">자세히</a>
                           </div>
                        </div>
                     </div>
                  </c:forEach>
               </div>
            </div>

            <div id="map" class="col-md-8 order-lg-2 showcase-text"></div>

         </div>
      </div>
   </section>
</div>








   <script
      src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
   <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE&callback=getMapData"></script>
   <script
      src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script>

       
   
   var map
    let houseIcon = new google.maps.MarkerImage("/static/img/house.png", null, null, null, new google.maps.Size(30,30));

    function initMap() {
       map= new google.maps.Map(
         document.getElementById('map'), 
         {
              zoom: 10,
              center: {
                 lat: parseFloat(locations[0].lat), lng: parseFloat(locations[0].lng)
              }
            }
      );

      // Create an array of alphabetical characters used to label the markers.
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

      // Add some markers to the map.
      // Note: The code uses the JavaScript Array.prototype.map() method to
      // create an array of markers based on a given "locations" array.
      // The map() method here has nothing to do with the Google Maps API.
      var markers = locations.map(
         function(location, i) {
              return new google.maps.Marker(
                    {
                         position: { lat: parseFloat(location.lat), lng: parseFloat(location.lng) },   // should be float, not string
                         label: labels[i % labels.length]
                    }
              );
            }
      );

      //Add a marker clusterer to manage the markers.
      var markerCluster = new MarkerClusterer(
         map,
         markers,
         {
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
         }
      );
    }
    
   var locations;
   
  
   function getMapData(){
      
      $.ajax(
      {
           type : 'get',
           url : '<%=contextPath%>/GoogleMapServlet',
           dataType : 'json',
           success : function(data, status, xhr) {
              locations = data;
              console.log(locations);
              initMap();
           }, 
           error: function(jqXHR, textStatus, errorThrown) 
           { 
              alert.notify(
                    'Opps!! 글 Map data를 받는 과정에 문제가 생겼습니다.', 
                    'error', //'error','warning','message'
                    3, //-1
                    function(){
                       console.log(jqXHR.responseText); 
                    }
                 );
           }
       });
   }
   $("#getSubwayList").click(function () {
      getSubwayList();               
   });
    
   $(document).on('click', ".line", function() {
      console.log("click");
      codeAddress($(this).children('p').text());      
   });
   function getSubwayList() {
      $.get("/test", 
         { 
         no: $("#no").val(),
         duration:$("#duration").val(),
         }, 
          function(data, status) { 
            console.dir(data);
            $("#station").empty();
            data.forEach(function(subway) {
               $("#station").append(`<button class="line" id="\${subway.no}" style="width: 15rem;"><a>\${subway.line}</a></br><a>\${subway.name}</a><p style="visibility:hidden">\${subway.address}</p></button>`)   
            });
            
          } 
      );
   }
   /* $(".line").click(function(){
      console.log("click");
      codeAddress($(this).children('p').text());
   });
 */
    function addMarker(tmpLat, tmpLng, name,iType ) {
        
           icon = houseIcon;
        
     
        var marker = new google.maps.Marker({
           position: new google.maps.LatLng(parseFloat(tmpLat),parseFloat(tmpLng)),
           label: name,
           title: name,    
           icon: icon
        });
        marker.addListener('click', function() {
           map.setZoom(18);
           map.setCenter(marker.getPosition());
        });
        marker.setMap(map);           
     }   

 
   function codeAddress(address) {
          geocoder = new google.maps.Geocoder();
           geocoder.geocode( { 'address': address}, function(results, status) {
             if (status == 'OK') {
              console.dir(results);
               map.setCenter(results[0].geometry.location);
               
               $.get("/houserest/geoList", 
                    { 
                    lat:results[0].geometry.location.lat,
               lng:results[0].geometry.location.lng,
                    }, 
                     function(data, status) { 
                       console.dir(data);
                       $("#housedealList").empty();
                       data.forEach(function(HouseDeal) {
                          addMarker(HouseDeal.lat,HouseDeal.lng,HouseDeal.aptName,1);
                          $("#housedealList").append(`
                                <div class="col-md-6">
                                 <div class="card" style="width: 15rem;">
                              <img src="<c:url value='/static/img/no_img.png'/>" class="card-img-top" style="height: 150px" alt="...">
                              <div class="card-body">
                                <p class="card-text">\${HouseDeal.dong}</p>
                                <h5 class="card-title">\${HouseDeal.dealAmount}만원</h5>                            
                                 <p class="card-text">\${HouseDeal.aptName}</p>
                               <a  href="<c:url value="/house/detail?no=\${HouseDeal.no}"/>">자세히</a>                    
                              </div>
                            </div> 
                         </div>   
                          `)   
                       });
                       
                     } 
                 );
               map.setZoom(17);                         
             } else {
               alert('Geocode was not successful for the following reason: ' + status);
             }
           });
         }
   
   
   
   // test?no=1&duration=100
   
      
   
   $(document).ready(function(){
         $.get("/line"      
            ,function(data, status){
               $.each(data, function(index, vo) {
                  $("#line").append("<option value='"+vo.line+"'>"+vo.line+"</option>");
               });//each
            }//function
            , "json"
         );//get
      });//ready
      
      $(document).ready(function(){
         $("#line").change(function() {
            $.get("/no"
                  ,{line:$("#line").val()}
                  ,function(data, status){
                     $("#no").empty();
                     $("#no").append('<option value="0">선택</option>');
                     $.each(data, function(index, vo) {
                        $("#no").append("<option value='"+vo.no+"'>"+vo.name+"</option>");
                     });//each
                  }//function
                  , "json"
            );//get
         });//change
      });
   
   
   
    </script>
<footer class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
          <ul class="list-inline mb-2">
            <li class="list-inline-item">
              <a href="#">About</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#">Contact</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
          </ul>
          <p class="text-muted small mb-4 mb-lg-0">&copy; Your Website 2019. All Rights Reserved.</p>
        </div>
        <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
          <ul class="list-inline mb-0">
            <li class="list-inline-item mr-3">
              <a href="#">
                <i class="fab fa-facebook fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item mr-3">
              <a href="#">
                <i class="fab fa-twitter-square fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-instagram fa-2x fa-fw"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
   <script src="<c:url value='/static/vendor/jquery/jquery.min.js'/>"></script>
  <script src="<c:url value= '/static/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>
</body>
</html>