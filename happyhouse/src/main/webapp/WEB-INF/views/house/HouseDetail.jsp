<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HappyHouse | 매물정보</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Bootstrap core CSS -->
<link
	href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link
	href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>"
	rel="stylesheet">
<link
	href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">


<!-- Custom styles for this template -->
<link href="<c:url value='/static/css/landing-page.min.css'/>"
	rel="stylesheet">


<!-- Google Maps JavaScript library -->
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE"></script>

<style>
* {
	font-family: "Spoqa Han Sans", Sans-serif;
	box-sizing: border-box;
	margin: 0px;
	padding: 0px;
}

info_header {
	width: 1180px;
	position: relative;
	margin: 0px auto;
}

ul, li {
	list-style: none;
}

li {
	display: list-item;
	text-align: -webkit-match-parent;
}

.cOrhka {
	color: rgb(34, 34, 34);
	font-size: 30px;
	line-height: 37px;
}

.hypste {
	margin-top: 8px;
	margin-left: 3px;
	color: rgb(102, 102, 102);
	font-size: 15px;
}

.fJuWIY {
	margin-bottom: 6px;
	color: rgb(102, 102, 102);
	font-size: 14px;
	line-height: 20px;
}

.items {
	display: flex;
	-webkit-box-align: center;
	align-items: center;
	width: 100%;
	padding-top: 35px;
	margin-bottom: 35px;
}

.item {
	height: 72px;
	padding-right: 25px;
	flex: 0 0 auto;
	border-right: 1px solid rgb(235, 235, 235);
}

p {
	display: block;
	margin-block-start: 1em;
	margin-block-end: 1em;
	margin-inline-start: 0px;
	margin-inline-end: 0px;
}

li>p {
	margin-bottom: 6px;
	color: rgb(102, 102, 102);
	font-size: 14px;
	line-height: 20px;
}

.item+li {
	margin-left: 25px;
}

ul {
	display: block;
	list-style-type: disc;
	margin-block-start: 1em;
	margin-block-end: 1em;
	margin-inline-start: 0px;
	margin-inline-end: 0px;
	padding-inline-start: 40px;
}

h1 {
	font-weight: 500;
}

.info_detial {
	width: 1180px;
	position: relative;
	margin: 0px auto;
}

.detail_items {
	width: 100%;
	margin-bottom: 30px;
	border-top: 2px solid rgb(34, 34, 34);
}

.detail_item {
	float: left;
	width: 25%;
	height: 50px;
	line-height: 50px;
	border-bottom: 1px solid rgb(235, 235, 235);
}

.info_name {
	float: left;
	width: 100px;
	color: rgb(136, 136, 136);
	font-size: 14px;
}

.info_item {
	float: left;
	color: rgb(34, 34, 34);
	font-size: 14px;
}

.default_div {
	width: 100%;
}

.imageview {
	width: 1180px;
	padding-top: 30px;
}

.imageviewBox {
	width: 1180px;
	position: relative;
	margin: 0px auto;
}

.info_no {
	display: inline-block;
	height: 42px;
	background-color: rgba(34, 34, 34, 0.7);
	position: absolute;
	top: 10px;
	left: 10px;
	z-index: 1;
	padding: 0px 17px;
	border-radius: 3px;
}

.info_no>p {
	color: rgb(255, 255, 255);
	font-size: 14px;
	text-align: center;
	line-height: 42px;
	margin-top: 0;
}

.imagesrc {
	width: 1180px;
	height: 420px;
	margin-bottom: 49px;
	position: relative;
	cursor: pointer;
}

.imagesrc>img {
	width: 100%;
	height: 100%;
	background-color: rgb(246, 247, 248);
	position: relative;
	border-width: 0px;
	border-style: initial;
	border-color: initial;
	border-image: initial;
}

.location {
	width: 1180px;
	position: relative;
	padding-top: 100px;
	padding-bottom: 20px;
	margin: 0px auto;
	border-top: 1px solid rgb(221, 221, 221);
}

.location_name {
	color: rgb(34, 34, 34);
	font-size: 28px;
	font-weight: 400;
	text-align: center;
	line-height: 41px;
	position: relative;
	z-index: 10;
}

.location_addr {
	color: rgb(102, 102, 102);
	font-size: 15px;
	line-height: 22px;
	text-align: center;
	margin: 3px 0px 27px;
}

.tab {
	display: flex;
	-webkit-box-align: center;
	align-items: center;
	-webkit-box-pack: center;
	justify-content: center;
	width: 850px;
	height: 56px;
	margin: 0px auto;
	border-bottom: 1px solid rgb(216, 216, 216);
}

.tab-content {
	margin: 20px, 0, 50px, 0;
	height: 100px;
}

.tab-items {
	width: 100%;
	margin-bottom: 30px;
}

.tab-item {
	float: left;
	width: 50px;
	height: 50px;
	line-height: 50px;
}

#favorite {
	font-size: 50px;
}
</style>

</head>
<body>
	<div class="wrap">
		<%@ include file="../header.jsp"%>
		<section class="container">
			<div class="info_header default_div">
				<ul class="items">
					<li class="item"><span id="favorite" onclick="addfavo()">★
					</span></li>
					<li class="item">
						<p>
							<span class="fJuWIY">${house.dong} ${house.aptName}</span>
							<input   type="button" value="복사" onclick="Copy();" /> <br />
							
						</p>
						<div>
							<h1 class="cOrhka">
								거래가 ${house.dealAmount}<span class="hypste">만원</span>
							</h1>
						</div>
					</li>
					<li class="item">
						<p>
							<span class="fJuWIY">전용 면적</span>
						<h1 class="cOrhka">${house.area}㎡</h1>
						</p>
					</li>

				</ul>
			</div>
			<div class="default_div">
				<div class="info_detail">
					<ul class="detail_items">
						<li class="detail_item">
							<p class="info_name">건물층</p>
							<div class="info_item">${house.floor}층</div>
						</li>
						<li class="detail_item">
							<p class="info_name">전용 면적</p>
							<div class="info_item">${house.area}㎡</div>
						</li>
						<li class="detail_item">
							<p class="info_name">건축연도</p>
							<div class="info_item">${house.buildYear}</div>
						</li>
						<li class="detail_item">
							<p class="info_name">거래연도/월/일</p>
							<div class="info_item">
								${house.dealYear}/${house.dealMonth}/${house.dealDay}</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="default_div">
				<div class="imageview">
					<div class="imageviewBox">
						<div class="info_no">
							<p>매물번호 ${house.no}</p>
						</div>
					</div>
					<div class="imagesrc">
						<img src="<c:url value='/static/img/no_img.png'/>" alt=""
							class="img-fluid">
					</div>
				</div>
			</div>
			<div class="default_div">
				<div class="location">
					<h1 class="location_name">위치 및 주변 시설</h1>
					<p class="location_addr">${house.dong}${house.aptName}</p>
				</div>
				<!-- map start -->
				<div class="text-center" style="margin: 20px, 0, 60px, 0">
					<div id="map" style="width: 100%; height: 400px"></div>
				</div>
				<div class="default_div">
					<ul class="nav nav-tabs"
						style="justify-content: center; align-items: center;">
						<li class="nav-item"><a class="nav-link active"
							data-toggle="tab" href="#a" id="market">편의시설</a></li>
						<li class="nav-item"><a class="nav-link" data-toggle="tab"
							href="#b" id="cctv">안전시설</a></li>

					</ul>
					<div class="tab-content">
						<div class="tab-pane fade show active" id="a">
							<div class="text-center">
								<ul class="tab-items">
									<li class="tab-item"><img
										src="<c:url value='/static/img/conveniencestore.png'/>"
										style="width: 44px; height: 44px; margin: 0px auto;">
										<p class="location_addr">편의점</p></li>
									<li class="tab-item"><img
										src="<c:url value='/static/img/coffee.png'/>"
										style="width: 44px; height: 44px; margin: 0px auto;">
										<p class="location_addr">카페</p></li>
								</ul>
							</div>
						</div>
						<div class="tab-pane fade" id="b">
							<div class="text-center" style="margin: 20px, 0, 50px, 0">
								<img src="<c:url value='/static/img/security-camera.png'/>"
									style="width: 44px; height: 44px; margin: 0px auto;">
								<p class="location_addr">CCTV</p>
							</div>
						</div>
					</div>
				</div>
				<script
					src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
				<script async defer
					src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE&callback=initMap"></script>
				<script src="<c:url value='/static/vendor/jquery/jquery.min.js'/>"></script>
				<script
					src="<c:url value= '/static/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>
				<script>
		var map;
	    var multi = {lat: 37.5665734, lng: 126.978179};
        let Lat = parseFloat(`${house.lat}`);
        let Lng = parseFloat(`${house.lng}`);
	    var markers = [];
	    let swLat;
        let swLng;
        let neLat;
        let neLng;
        let check = true; //true 편의시설, false 안전시설
	   //아이콘
	   let cctvIcon = new google.maps.MarkerImage("/static/img/security-camera.png", null, null, null, new google.maps.Size(30,30));
	   let coffeeIcon = new google.maps.MarkerImage("/static/img/coffee.png", null, null, null, new google.maps.Size(30,30));		   
	   let convenienceIcon = new google.maps.MarkerImage("/static/img/conveniencestore.png", null, null, null, new google.maps.Size(30,30));
	   let houseIcon = new google.maps.MarkerImage("/static/img/house.png", null, null, null, new google.maps.Size(30,30));
		   

		   function initMap() {
		      if(Lat!=0){
			     map = new google.maps.Map(document.getElementById('map'),{
			    	  center:{lat : Lat, lng: Lng},
			          zoom : 18
			      });
	 		      addMarker(Lat,Lng,`${house.aptName}`,4)
		      }else{
			      map = new google.maps.Map(document.getElementById('map'),{
				        center: multi, zoom: 15
			      });
			      let addr = `${house.dong}+${house.aptName}+${house.jibun}`;
			      geocode(addr);		    	  
		      }		
		      if(check){
			      marketSelect();		    	  
		      }else{
		    	  cctvSelect();
		      }
		      
		      google.maps.event.addListenerOnce(map, 'bounds_changed', function() {		    	  
		    	   swLat = parseFloat(map.getBounds().getSouthWest().lat());
		           swLng = parseFloat(map.getBounds().getSouthWest().lng());
		           neLat = parseFloat(map.getBounds().getNorthEast().lat());
		           neLng = parseFloat(map.getBounds().getNorthEast().lng());       
		       
		           if(!check){
		        	   cctvSelect(); 
		           } 		           
		       });
		   }
		   
		   function addMarker(tmpLat, tmpLng, name,iType ) {
			  //1:편의점 2: 카페 3:cctv 
			  if(iType==1){
				  icon = convenienceIcon;
			  }else if(iType==2){
				  icon = coffeeIcon;
			  }else if(iType==3){
				  icon = cctvIcon;
			  }else{
				  icon = houseIcon;
			  }
		   
		      var marker = new google.maps.Marker({
		         position: new google.maps.LatLng(parseFloat(tmpLat),parseFloat(tmpLng)),
		         label: name,
		         title: name, 	
		         icon: icon
		      });
		      marker.addListener('click', function() {
		         map.setZoom(18);
		         map.setCenter(marker.getPosition());
		      });
		      marker.setMap(map);		     
		   }		
		   
		   function deleteMarkers() {
			   map= new google.maps.Map(document.getElementById('map'), {
			    	  center:{lat : Lat, lng: Lng},
					  zoom: 18
			      });
	 		      addMarker(Lat,Lng,`${house.aptName}`,4);
		     }
		   
	      function geocode(addr) {             
          $.get("https://maps.googleapis.com/maps/api/geocode/json"
                ,{   key:'AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE'
                   , address:addr
                }
                , function(data, status) {
                      map.setCenter(data.results[0].geometry.location);
                      map.setZoom(18);
                   	  addMarker(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng, `${house.aptName}`,4);
                }
                , "json"
          );//get		          
	       }
		function cctvSelect(){
			$.get("/cctv"
		            ,{swLat: swLat,
		              swLng: swLng,
		              neLat: neLat,
		              neLng: neLng},
		              function(data,status){
				        $.each(data, function(index, vo) {
				        	addMarker(vo.lat,vo.lng,vo.usetype,3);
				        });	            		            	  
		              },
		              "json"
	      		);//get
		}
		function marketSelect(){
			$.get("/market"
		            ,{dong: `${house.dong}`},
		              function(data,status){
				        $.each(data, function(index, vo) {
				        	console.dir(vo);
				        	if(vo.smallCategory == '편의점'){
					        	addMarker(vo.lat,vo.lng,vo.shopname,1);					        		
				        	}else if(vo.smallCategory== '커피전문점/카페/다방'){
					        	addMarker(vo.lat,vo.lng,vo.shopname,2);
				        	}					        	
				        });	            		            	  
		              },
		              "json"
	      		);//get
		}
		
		$('#market').on('click',function(event){
			deleteMarkers();
			marketSelect();
			check = true;

		})
		$('#cctv').on('click',function(){
			deleteMarkers();
			cctvSelect();
			check = false;
		});
		
		
	    const house = sessionStorage.getItem('house');
	    let newHouse = {
	    		sequence:0,
	    		items:[],
	    }
	    if(house){
	    	newHouse = JSON.parse(house);
	    }
	    newHouse.sequence++;	    
	   
	    newHouse.items.push(${jsonHouse});
	    console.dir(newHouse);
	    sessionStorage.setItem('house',JSON.stringify(newHouse));
	 	 //즐겨찾기 추가
		function addfavo(){
	 		 var login = "${loginUser}";
	 		 if(!login){
	 			 alert("로그인 하세요");
	 		 }else{	 			 
				 $.ajax({
			          url: '/favorite/addfavo',
			          type: 'POST',
			          data:{
			        	  "id":"${loginUser.userId}",
			        	  "no":${house.no},
			        	  "AptName":"${house.aptName}",
			        	  "dealAmount":"${house.dealAmount}"    	  
			          } ,
			          success: function (data) {
							alert(data);
			          },
			          error:function(){
			        	  alert("등록 실패");
			          }
			        });
				}
	 		 }	 		 
		function Copy() {
			  var Url = document.getElementById("url");
			  var t = document.createElement("textarea");
			  document.body.appendChild(t);
			  t.value = window.location.href;
			  t.select();
			  document.execCommand('copy');
			}

		</script>
			</div>
		</section>
	</div>


	<footer class="bg-light">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 h-100 text-center text-lg-left my-auto">
					<ul class="list-inline mb-2">
						<li class="list-inline-item"><a href="#">About</a></li>
						<li class="list-inline-item">&sdot;</li>
						<li class="list-inline-item"><a href="#">Contact</a></li>
						<li class="list-inline-item">&sdot;</li>
						<li class="list-inline-item"><a href="#">Terms of Use</a></li>
						<li class="list-inline-item">&sdot;</li>
						<li class="list-inline-item"><a href="#">Privacy Policy</a></li>
					</ul>
					<p class="text-muted small mb-4 mb-lg-0">&copy; Your Website
						2019. All Rights Reserved.</p>
				</div>
				<div class="col-lg-6 h-100 text-center text-lg-right my-auto">
					<ul class="list-inline mb-0">
						<li class="list-inline-item mr-3"><a href="#"> <i
								class="fab fa-facebook fa-2x fa-fw"></i>
						</a></li>
						<li class="list-inline-item mr-3"><a href="#"> <i
								class="fab fa-twitter-square fa-2x fa-fw"></i>
						</a></li>
						<li class="list-inline-item"><a href="#"> <i
								class="fab fa-instagram fa-2x fa-fw"></i>
						</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>



</body>
</html>