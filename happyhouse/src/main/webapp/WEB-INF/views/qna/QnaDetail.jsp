<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>HappyHouse | 문의사항</title>
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
    <!-- Bootstrap core CSS -->
  	<link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

  	<!-- Custom fonts for this template -->
  	<link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  	<link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  	<!-- Custom styles for this template -->
  	<link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">
	
</head>
<body>	
	<div class="wrap">
	<%@ include file="../header.jsp" %>
	
	<div class="container">	
		<div class="row">
			<div class="col-lg-8 mt-5 center-block" >
				<h2>문의사항</h2>	
				<div align="right">		
					<c:if test="${loginUser.userId eq qna.qnaUserid || loginUser.userId eq 'admin'}">
						<a href="update?no=${qna.qnaNo}">수정</a>
						<a href="delete?no=${qna.qnaNo }">삭제</a>
						
					</c:if>
				</div>
				<table class="table table-hover">		
					<tr>
						<th scope="col">글번호</th>
						<td>${qna.qnaNo}</td>
					</tr>
					<tr>
						<th scope="col">제목</th>
						<td>${qna.qnaTitle}</td>
					</tr>
					<tr>
						<th scope="col">작성자</th>
						<td>${qna.qnaUserid}</td>
					</tr>
					<tr>
						<th scope="col">작성일</th>
						<td>${qna.qnaDatetime}</td>
					</tr>
					<tr>
						<th scope="col">내용</th>
						<td>${qna.qnaContent}</td>
					</tr>					
				</table>
			</div>				
		</div>
		<div class="row">
			<div class="col-lg-8 mt-5 center-block" >
				<c:choose>
					<c:when test="${empty qna.replyContent  }">
						<c:if test="${ loginUser.userId eq 'admin' }">
							<form method="post" action="<c:url value="/qna/reply"/>">				
								<label>답변 내용 </label>
								<input type="hidden" name="replyUserid" value="${loginUser.userId }">
								<input type="hidden" name="qnaNo" value="${qna.qnaNo }">
								<textarea class="form-control col-15" rows="5" name="replyContent"></textarea>
								<button id="replyBtn">작성</button>									
							</form>		
						</c:if>			
					</c:when>				
					<c:when test="${not empty qna.replyContent}">					
						<div class="reply">
							<label>답변 내용 </label>
							<textarea class="form-control col-15" rows="5" id="replyContent" readonly>
								${qna.replyContent }
							</textarea>																
						</div>							
					</c:when>				
				</c:choose>			
			</div>
		</div>		
	</div>
	</div>
	
	<%@ include file="../footer.jsp" %>	
</body>
</html>
