<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>HappyHouse | 공지사항</title>
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
  	<!-- Bootstrap core CSS -->
  	<link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

  	<!-- Custom fonts for this template -->
  	<link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  	<link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  	<!-- Custom styles for this template -->
  	<link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">	
  	
</head>
<body>
	<div class="wrap">
		<%@ include file="../header.jsp" %>
		<div class="container">	
			<div class="row">
				<div class="col-lg-8 mt-5 center-block" >
					<h2>문의사항 등록</h2>					
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 mt-5 center-block" >
					<form method="post" action="<c:url value="/qna/write" />">		
					  <div class="form-group" align="left">
						<label>제목 : </label>
						<input type="text" class="form-control" name="qnaTitle" />
					  </div>
					  <div class="form-group" align="left">
						<label>작성자 : </label>
						<input type="text" class="form-control" name="qnaUserid" value="${loginUser.userId}" readonly="readonly" />
					  </div>
					  <div class="form-group" align="left">
						<label>내용 : </label>
						<textarea class="form-control col-15" rows="5" name="qnaContent"></textarea>
						
					  </div>
					  <div class="form-group" align="left">
					  	<button type="submit" class="btn btn-warning">등록</button>
					  	<button type="reset" class="btn btn-primary">취소</button>
					  </div>
					</form>				
				</div>
			</div>
		</div>	
	</div>
	<%@ include file="../footer.jsp" %>
	
</body>
</html>