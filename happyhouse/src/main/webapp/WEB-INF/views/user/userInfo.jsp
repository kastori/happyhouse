<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
	<title>HappyHouse | 내정보</title>

	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
  	<!-- Bootstrap core CSS -->
  	<link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

  	<!-- Custom fonts for this template -->
  	<link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  	<link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  	<!-- Custom styles for this template -->
  	<link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">	
  	
</head>
<body>	
	<div class="wrap">	
		<%@ include file="../header.jsp" %>
			<div class="container">
			    <div class="row ">
			        <div class="col-6 mx-auto">
			            <div class="jumbotron">
							<form class="from-group" method="post" action="<c:url value="/user.do"/>">
								<input type="hidden" name="act" value="signup"/>
									<div>
										<div>
											<label for="InputId">ID:</label>
											<a>${loginUser.userId}</a>
										</div>
										<div>
											<label for="InputName">이름:</label>
											<a>${loginUser.userName}</a>
										</div>
										<div>
											<label for="InputEmail">이메일:</label>
													<a>${loginUser.email}</a>
										</div>
										<div>
											<label for="InputAddress">주소:</label>
												<a>${loginUser.address}</a>
										</div>
										<div>
											<label for="InputTelephone">전화번호:</label>
												<a>${loginUser.telephone}</a>
										</div>
									</div>
							</form>
						</div>
		     	  </div>
		    </div>
		    <div class="row">
		    	<div class="container-fluid p-0">
					<div class="row" id=favo>
			
					</div>
				</div>		
		    </div>
		</div>		
	</div>
	<%@ include file="../footer.jsp" %>
	<script type="text/javascript">
		getfavo();
		function getfavo(){
			
			
			$.get("/favorite/getfavo", 
					{ 
						id:"${loginUser.userId}"
					}, 
					 function(data, status) { 
						data.forEach(function(Favorite) {
							$("#favo").append(`
				      	  			<div class="col-md-3">
									<div class="card" style="width: 15rem;">
								  <img src="<c:url value='/static/img/no_img.png'/>" class="card-img-top" style="height: 150px" alt="...">
								  <div class="card-body">
							   		<h5 class="card-title">\${Favorite.dealAmount}만원</h5>								    
								  	<p class="card-text">\${Favorite.aptName}</p>
									<a  href="<c:url value="/favo/deletefavo?fid=\${Favorite.fid}"/>">삭제</a>						  
									<a  href="<c:url value="/house/detail?no=\${Favorite.no}"/>">자세히</a>						  
								  </div>
								</div> 
							`)	
						});
						
					 } 
				);	
			}		
		</script>
</body>
</html>