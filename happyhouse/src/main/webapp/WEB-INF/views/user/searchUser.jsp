<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<title>HappyHouse | 유저검색</title>
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
    <!-- Bootstrap core CSS -->
  	<link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

  	<!-- Custom fonts for this template -->
  	<link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  	<link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  	<!-- Custom styles for this template -->
  	<link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">
</head>
<body>
	<%@ include file="../header.jsp" %>

	<c:choose>
		<c:when test="${empty searchUser}">
			<div>
				${errMsg}
			</div>
		</c:when>
		<c:otherwise>
		
		
				<div class="container h-100">
				    <div class="row align-items-center h-100">
				        <div class="col-6 mx-auto">
				            <div class="jumbotron">
								<form class="from-group" method="post" action="<c:url value="/user.do"/>">
									<input type="hidden" name="act" value="signup"/>
										<div>
											<div>
												<label for="InputId">ID:</label>
												<a>${searchUser.userId}</a>
											</div>
											<div>
												<label for="InputName">이름:</label>
												<a>${searchUser.userName}</a>
											</div>
											<div>
												<label for="InputEmail">이메일:</label>
														<a>${searchUser.email}</a>
											</div>
											<div>
												<label for="InputAddress">주소:</label>
													<a>${searchUser.address}</a>
											</div>
											<div>
												<label for="InputTelephone">전화번호:</label>
													<a>${searchUser.telephone}</a>
											</div>
											
										</div>
								</form>
							</div>
			     	  </div>
			    </div>
			</div>
			
			</c:otherwise>
		</c:choose>
		<div>
		
	</div>
	
		
</body>
</html>