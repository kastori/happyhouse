<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<title>HappyHouse | 로그인</title>
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
    <!-- Bootstrap core CSS -->
  	<link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

  	<!-- Custom fonts for this template -->
  	<link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  	<link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  	<!-- Custom styles for this template -->
  	<link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">
</head>
<body>
	<div class="wrap">
		<%@ include file="../header.jsp" %>

		<div class="container ">
			<div class="row align-items-center h-100">
		        <div class="col-6 mx-auto mt-50">
		            <div class="jumbotron">
						<form class="from-group" method="post" action=	"<c:url value="/login"/>">
							<input type="hidden" name="act" value="login"/>
							<div>
								<div>
									<label for="InputId">ID</label>
										<input type="text" class="form-control" name="userId" id="InputId"/>
								</div>
								<div>
									<label for="InputPwd">암호</label>
									<input type="password" class="form-control" name="userPwd" id="InputPwd"/>
								</div>
								<div>
									<button type="submit" class="btn btn-primary">로그인</button>
									 <a href="<c:url value ="/User/signupForm"/>">회원가입</a>
								</div>										
							</div>
						</form>
					</div>
	     	  </div>
		    </div>
		</div>
	</div>
	<%@ include file="../footer.jsp" %>
</body>
</html>