<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>HappyHouse | 공지사항</title>
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
  	<!-- Bootstrap core CSS -->
  	<link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

  	<!-- Custom fonts for this template -->
  	<link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  	<link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  	<!-- Custom styles for this template -->
  	<link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">	
  	
</head>
<body>
	<div class="wrap">
		<%@ include file="../header.jsp" %>
		<div class="container">	
			<div class="row">
				<div class="col-lg-8 mt-5 center-block" >			
					<h2>공지 상세 보기</h2>	
				</div>
			</div>
			<div class="row">				
				<table class="table table-hover">		
					<tr>
						<th scope="col">글번호</th>
						<td>${notice.noticeNo}</td>
					</tr>
					<tr>
						<th scope="col">제목</th>
						<td>${notice.noticeTitle}</td>
					</tr>
					<tr>
						<th scope="col">작성자</th>
						<td>${notice.noticeWriter}</td>
					</tr>
					<tr>
						<th scope="col">작성일</th>
						<td>${notice.regDate}</td>
					</tr>
					<tr>
						<th scope="col">내용</th>
						<td>${notice.noticeContent}</td>
					</tr>				
				</table>				
				<div>		
					<c:if test="${loginUser.userId eq 'admin'}">			
						<a href="modify?noticeNo=${notice.noticeNo}">수정</a>
						<a href="remove?noticeNo=${notice.noticeNo }">삭제</a>
					</c:if>
				</div>
			</div>
			
		</div>
	</div>
	
	<%@ include file="../footer.jsp" %>	
</body>
</html>
