<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>HappyHouse | 공지사항</title>	
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
    <!-- Bootstrap core CSS -->
  	<link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

  	<!-- Custom fonts for this template -->
  	<link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  	<link href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  	<!-- Custom styles for this template -->
  	<link href="<c:url value='/static/css/landing-page.min.css'/>" rel="stylesheet">
	<style>
		.pagination {
		   justify-content: center;
		   text-align:center;
		}
		.row{
			maring:0;
		}
		
	</style>
</head>
<body>
	<div class="wrap">	
		<%@ include file="../header.jsp" %>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mt-5 center-block" >							
	  				<h2>공지사항</h2>
  				</div>
			</div>
			<div class="row">
				<table class="table table-hover">
					<tr>
						<th scope="col">글번호</th>
						<th scope="col">제목</th>
						<th scope="col">작성자</th>
						<th scope="col">조회수</th>
						<th scope="col">작성일</th>
					</tr>
					<c:forEach var="notice" items="${list}">
						<tr>
							<td scope="row">${notice.noticeNo}</td>
							<td><a href="<c:url value="/notice/detail?noticeNo=${notice.noticeNo}" />">${notice.noticeTitle}</a></td>
							<td>${notice.noticeWriter}</td>
							<td>${notice.noticeView}</td>
							<td>${notice.regDate}</td>
						</tr>
					</c:forEach>
				</table>  	
		  	</div>
		  	<div class="row">		
		  		<div class="col-lg-10 mt-10 center-block" >		  			  			  	
		  	  			  	
					<nav class="justify-content-center">
			  			 <ul class="pagination ">
					      <c:if test="${pageMaker.prev}">
					    	 <li class="page-item">
					    		<a href="list${pageMaker.makeSearch(pageMaker.startPage - 1)}"  aria-label="Previous" class="page-link">
									<span aria-hidden="true">&laquo;</span>
									<span class="sr-only">Previous</span>								
								</a>
					    	</li>
					    </c:if> 
					
					    <c:forEach begin="${pageMaker.startPage}" end="${pageMaker.endPage}" var="idx">
					    	 <li class="page-item"><a href="list${pageMaker.makeSearch(idx)}" class="page-link">${idx}</a></li>
					    </c:forEach>
					
					    <c:if test="${pageMaker.next && pageMaker.endPage > 0}">
					    	 <li class="page-item">
					    		<a href="list${pageMaker.makeSearch(pageMaker.endPage + 1)}" aria-label="Next" class="page-link">
			     				   <span aria-hidden="true">&raquo;</span>
					    		</a>
					    	</li>
					    </c:if> 
					  </ul>
					</nav>
				</div>
		  	</div> 	
		  	<div class="row">
		  		<div class="col-lg-10 mt-10 center-block" >		  			  			  	
 		  		 	<div class="form-inline justify-content-center">
 		  		 		<div class="form-inline ">		  		
							<div class="search form-group" >
						 		<select class="form-control" name="searchType">
							      <option value="n"<c:out value="${scri.searchType == null ? 'selected' : ''}"/>>-----</option>
							      <option value="t"<c:out value="${scri.searchType eq 't' ? 'selected' : ''}"/>>제목</option>
							      <option value="c"<c:out value="${scri.searchType eq 'c' ? 'selected' : ''}"/>>내용</option>
							      <option value="w"<c:out value="${scri.searchType eq 'w' ? 'selected' : ''}"/>>작성자</option>
							      <option value="tc"<c:out value="${scri.searchType eq 'tc' ? 'selected' : ''}"/>>제목+내용</option>
							    </select>			
							    <input class="form-control" type="text" name="keyword" id="keywordInput"  value="${scri.keyword}"/>
							
							    <button  class="form-control"id="searchBtn" type="button">검색</button>
							    <script>
							      $(function(){
							        $('#searchBtn').click(function() {
							          self.location = "list" + '${pageMaker.makeQuery(1)}' + "&searchType=" + $("select option:selected").val() + "&keyword=" + encodeURIComponent($('#keywordInput').val());
							        });
							      });   
							    </script>
								<c:if test="${loginUser.userId eq 'admin'}">   
						        	<button type="button" class="btn btn-primary" onclick="location.href='<c:url value="/notice/regist" />'">글쓰기</button>
						     	</c:if>
					 	 	</div>	  	
				  		</div>	 	
 		  		 	</div>
 		  		 </div>	  		
	  		</div>
		  </div>	
	</div>		
	 <!-- Footer -->
	<%@ include file="../footer.jsp"%>

	
</body>
</html>