<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>HappyHouse</title>

<!-- slick -->
<link rel="stylesheet" type="text/css"
	href="<c:url value='/static/css/slick/slick.css'/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/static/css/slick/slick-theme.css'/>">

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<!-- Bootstrap core CSS -->
<link
	href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link
	href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>"
	rel="stylesheet">
<link
	href="<c:url value='/static/vendor/simple-line-icons/css/simple-line-icons.css'/>"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">

<!-- Custom styles for this template -->
<link href="<c:url value='/static/css/landing-page.min.css'/>"
	rel="stylesheet">

<!-- Google Maps JavaScript library -->
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCGrOx3Wqj1azG7dgY5eoZ9BOAsBCVQGhE"></script>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<style>
#search_input {
	font-size: 18px;
}

#form-group input:focus {
	color: #495057;
	background-color: #fff;
	border-color: #80bdff;
	outline: 0;
	box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, .25);
}

.slider {
	width: 80%;
	margin: 100px auto;
}

.slick-slide {
	margin: 0px 20px;
}

.slick-slide img {
	width: 100%;
}

.slick-prev:before, .slick-next:before {
	color: black;
}

.slick-slide {
	transition: all ease-in-out .3s;
	opacity: .2;
}

.slick-active {
	opacity: 1;
}

.slick-current {
	opacity: 1;
}

.w3-card-4 {
	height: 100px;
	text-align: center;
	vertical-align: middle
}
</style>
</head>
<body>
	<div class="wrap">
		<!--  Navigation -->
		<%@ include file="header.jsp"%>
		<!-- Header -->
		<header class="masthead text-white text-center">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-xl-9 mx-auto">
						<div class="mb-5">
							<p>
								<span><h1>어떤 아파트, 어떤 동네에서</h1></span>
							</p>
							<p>
								<span><h1>살고 싶으신가요?</h1></span>
							</p>
						</div>
					</div>
					<div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
						<form method="get" action="<c:url value="/house/list"/>">
							<div class="form-row" id="from group">
								<div class="col-12 col-md-9 mb-2 mb-md-0">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
									<input type="text" class="form-control form-control-lg"
										id="search_input" name="keyword" placeholder="아파트 이름, 동을 적으세요">
									<input type="hidden" name="select" value="1"> <input
										type="hidden" name="select" value="2"> <input
										type="hidden" name="select" value="3"> <input
										type="hidden" name="select" value="4"> <input
										type="hidden" name="searchType" value="ad">

								</div>
								<div class="col-12 col-md-3">
									<button type="submit" class="btn btn-block btn-lg btn-primary">검색</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--end row  -->
			</div>
			<!--end container  -->
		</header>
		<!-- Icons Grid -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12 mt-20 text-center">
					<h3>최근 본 매물</h3>
				</div>

			</div>
			<div class="row">
				<div class="slider"></div>
			</div>
		</div>
		<!-- Image Showcases -->
		<section class="showcase">
			<div class="container-fluid p-0">
				<div class="row no-gutters">
					<div class="col-lg-6 order-lg-2 my-auto showcase-text">
						<h2>공지사항</h2>
						<hr>
						<c:forEach var="notice" items="${list}">
							<li><a
								href="<c:url value="/notice/detail?noticeNo=${notice.noticeNo}" />">${notice.noticeTitle}</a>
							</li>
						</c:forEach>

					</div>
					<div class="col-lg-6 order-lg-1 my-auto showcase-text">
						<h2>최근 뉴스</h2>
						<hr>
						<c:forEach var="h" items="${href }" varStatus="status">
							<li><a href="${h}" target="_blank">${title[status.index]}</a>
							</li>
						</c:forEach>
					</div>
				</div>
			</div>
		</section>

		<script src="<c:url value='/static/js/slick/slick.js'/>"
			type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript"
			src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

		<!-- <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
 -->
		<script>
			var $jq = jQuery.noConflict();
			const house = sessionStorage.getItem('house');
			let newHouse = {
				sequence : 0,
				items : [],
			}
			if (house) {
				newHouse = JSON.parse(house);
			} else {
				$jq('.slider')
						.append(
								'<div>최근 본 매물이 없습니다</div><div>최근 본 매물이 없습니다</div><div>최근 본 매물이 없습니다</div>');
			}

			let items = newHouse.items;
			for (var i = items.length - 1; i > 0; i--) {
				var temp = '<div class="w3-card-4"> <div class="w3-container w3-center"><a href="/house/detail?no='
						+ items[i].no
						+ '"> <p>'
						+ items[i].dong
						+ ' '
						+ items[i].aptName + '</p></a></div></div>';
				$jq('.slider').append(temp);

			}

			$jq(document).ready(function() {
				$jq('.slider').slick({
					infinite : true,
					dots : true,
					slidesToShow : 3,
					slidesToScroll : 3
				});

			});

			var searchInput = 'search_input';

			$jq(document).ready(
					function() {
						var autocomplete;
						autocomplete = new google.maps.places.Autocomplete(
								(document.getElementById(searchInput)), {
									types : [ 'geocode' ],
								/*componentRestrictions: {
								 country: "USA"
								}*/
								});

						google.maps.event.addListener(autocomplete,
								'place_changed', function() {
									var near_place = autocomplete.getPlace();
								});
					});
		</script>


	</div>


	<!-- Footer -->
	<%@ include file="footer.jsp"%>

</body>
</html>