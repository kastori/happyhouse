<!-- PROJECT LOGO -->
<br />
<p align="center">

  <h2 align="center">HappyHouse</h2>

  <p align="center">
        좀더 쉬운 집 찾기
    <br />
</p>

<br>

<!-- ABOUT THE PROJECT -->

## :paperclip: 프로젝트 소개

> 좀더 쉬운 집 찾기!!!

&nbsp; 1인가구는 점차 증가하고 있고 수도권 지역의 원룸 수요는 늘어나고 있습니다. 하지만, 실제 회사가 위치하는 서울의 월세는 감당하기 힘들만큼 비쌉니다. 따라서, 저희는 회사를 중심으로 출퇴근 가능한 거리의 주택정보를 제공하는 기능을 제공하려고 합니다.<br>
&nbsp; HappyHouse는 지하철 역명을 기반으로 특정 시간안에 출퇴근 가능한 지역을 알려주는 차별화된 기능을 가지고 있습니다.
<br>

### :clipboard: 프로젝트 산출물

[Happy House 발표자료](./doc/최종PPT.pptx) <br>
[Happy Housse 설계서](./doc/관통%20프로젝트%20설계서.docx)

<h3> - Usecase Diagram</h3>

![Usecase DiaGram](./doc/img/UsecaseDiagram1.png)![Usecase DiaGram](./doc/img/UsecaseDiagram2.png)<br>

<h3> - ERD</h3>

![ERD](./doc/img/ERD.png)

<h3> - ClassDiagram </h3>

![ClassDiagram](./doc/img/classDiagram.png)

<h3> - 화면설계서</h3>

![화면설계서](./doc/img/화면설계서.png)

### :clipboard: 기능 소개

<h3> 메인화면</h3>

![메인화면](doc/img/메인화면1.png)!

<h3>장소 자동 완성</h3>

![뉴스크롤링](doc/img/메인화면2.png)<br>

<h3>최신 부동산 뉴스 Top10(다음 부동산) 크롤링</h3>

![뉴스크롤링](./doc/img/메인화면3.png)

<h3> 최근 본 매물</h3>

![최근본매물](./doc/img/메인화면4.png)

<h3>아파트 검색</h3>

![아파트 검색](./doc/img/아파트검색.png)

<h3>상세페이지</h3>

![상세페이지](./doc/img/아파트검색상세페이지.png)

<h3>지하철 거리기반 검색</h3>

![아파트 검색](./doc/img/지하철거리기반검색.png)

<h3>문의사항</h3>

![문의사항](./doc/img/문의사항.png)

### Built With

<br>

<!-- GETTING STARTED -->

## :gear: Getting Started

HappyHouse 시작 방법입니다.

### Installation

1. Clone the repo

```sh
git clone https://gitlab.com/kastori1990/happyhouse.git
```

2. 백엔드 실행

```sh
cd happyhouse
mvn spring-boot:run
```

<br>

## :hammer_and_pick: 개발환경

Languege

- [JQuery](https://jquery.com/)
- [Java](https://java.com/ko/download/)
- JSP

DB

- [MariaDB](https://mariadb.org/)

Library

- [Mybatis](https://mybatis.org/mybatis-3/)
- [Google Map](https://cloud.google.com/maps-platform/maps)
- [Google Places](https://cloud.google.com/maps-platform/places)
- [slick.js](https://kenwheeler.github.io/slick/)

Framework

- [Spring Boot](https://spring.io/projects/spring-boot)

ETC

- RestfulApi
- [Swagger](https://swagger.io/)
  <br>

<!-- CONTACT -->

## :busts_in_silhouette: Contact

- `김영민` - kastori1990@gmail.com<br>
- `이원우` - dddfff22@gmail.com<br>

Project Link: [https://gitlab.com/kastori1990/happyhouse](https://gitlab.com/kastori1990/happyhouse)

<br>
